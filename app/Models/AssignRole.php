<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AssignRole extends Model
{
    protected $table = 'assign_roles';
    protected $guarded = [];

    public function masterAdjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'assign_type_id', 'id');
    }
}
