<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class AdjusterCertificate extends Model
{
    protected $table = 'adjuster_certificates';
    protected $guarded = [];

    public function setExpiryDateAttribute($value){
        $datetime = $value == null ? null : DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['expiry_date'] = $datetime->format('Y-m-d');
    }

    public function getExpiryDateAttribute($value){
        return Carbon::parse($value)->format('m-d-Y');
    }
}
