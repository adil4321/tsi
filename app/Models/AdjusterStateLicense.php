<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class AdjusterStateLicense extends Model
{
    protected $table = 'adjuster_state_licenses';
    protected $guarded = [];

    public function state(){
        return $this->belongsTo(State::class, 'state_id', 'id');
    }

    public function adjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'master_adjuster_id', 'id');
    }

    public function setExpiryDateAttribute($value){
        $datetime = DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['expiry_date'] = $datetime->format('Y-m-d');
    }

    public function getExpiryDateAttribute($value){
        return Carbon::parse($value)->format('m-d-Y');
    }
}
