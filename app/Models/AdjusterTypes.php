<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdjusterTypes extends Model
{
    protected $table = 'adjuster_types';
    protected $guarded = [];
}
