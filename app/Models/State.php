<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class State extends Model
{
    protected $table = 'states';
    protected $guarded = [];

    public function master_adjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'state_id', 'id');
    }

    public function cities() : HasMany{
        return $this->hasMany(City::class, 'state_id', 'id');
    }

}
