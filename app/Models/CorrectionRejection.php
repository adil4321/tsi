<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CorrectionRejection extends Model
{
    protected $table = 'correction_rejections';
    protected $guarded = [];

    public function field_adjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'adjuster_id', 'id');
    }

    public function carrier(){
        return $this->belongsTo(Carrier::class, 'carrier_id', 'id');
    }

    public function leader(){
        return $this->belongsTo(Leader::class, 'leader_id', 'id');
    }

    public function qa(){
        return $this->belongsTo(Qa::class, 'qa_id', 'id');
    }

    public function setReviewDateAttribute($value){
        return $this->attributes['review_date'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getReviewDateAttribute($value){
        return Carbon::parse($value)->format('m-d-Y');
    }
}
