<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdjusterTypeMasterAdjuster extends Model
{
    protected $table = 'adjuster_type_master_adjuster';
    protected $guarded = [];

    public function master_adjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'master_adjuster_id', 'id');
    }

    public function adjuster_type(){
        return $this->belongsTo(AdjusterTypes::class, 'adjuster_type_id', 'id');
    }
}
