<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdjusterGrade extends Model
{
    protected $table = 'adjuster_grades';
    protected $guarded = [];

    public function master_adjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'adjuster_grade_id', id);
    }
}
