<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClAssignType extends Model
{
    protected $table = 'cl_assign_types';
    protected $guarded = [];
}
