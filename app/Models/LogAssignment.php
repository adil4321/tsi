<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class LogAssignment extends Model
{
    protected $table = 'log_assignments';
    protected $guarded = [];

    // public function carrier(){
    //     return $this->belongsTo(Carrier::class, 'carrier_id', 'id');
    // }

    // public function field_assign_source(){
    //     return $this->belongsTo(FieldAssignSource::class, 'field_assign_source_id', 'id');
    // }

    public function master_adjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'master_adjuster_id', 'id');
    }

    // public function cl_assign_type(){
    //     return $this->belongsTo(ClAssignType::class, 'cl_assign_type_id', 'id');
    // }

    public function state(){
        return $this->belongsTo(State::class, 'state_id', 'id');
    }

    public function city(){
        return $this->belongsTo(City::class, 'city_id', 'id');
    }

    // public function close_type(){
    //     return $this->belongsTo(CloseType::class, 'close_type_id', 'id');

    // }

    public function setDateAttribute($value){
        //$datetime = new DateTime($value);
        $datetime = $value == null ? null : DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['date'] = $datetime->format('Y-m-d');
    }

    public function getDateAttribute($value){
        return Carbon::parse($value)->format('m-d-Y');
    }
}
