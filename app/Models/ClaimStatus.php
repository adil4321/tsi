<?php

namespace App\Models;

use Carbon\Carbon;
use DateTime;
use Illuminate\Database\Eloquent\Model;

class ClaimStatus extends Model
{
    protected $table = "claim_status";
    protected $guarded = [];

    public function master_adjuster(){
        return $this->belongsTo(MasterAdjuster::class, 'master_adjuster_id', 'id');
    }

    public function setDateReceivedAttribute($value){
        $datetime = $value == null ? null : DateTime::createFromFormat('m-d-Y', $value);
        
        return $this->attributes['date_received'] = $datetime->format('Y-m-d');
    }

    public function getDateReceivedAttribute($value){
        return Carbon::parse($value)->format('m-d-Y');
    }

    public function setDateOfNoticeAttribute($value){
        //$datetime = new DateTime($value);
        $datetime = $value == null ? null : DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['date_of_notice'] = $datetime->format('Y-m-d');
    }

    public function getDateOfNoticeAttribute($value){
        return Carbon::parse($value)->format('m-d-Y');
    }

    public function setDateContactedAttribute($value){
        //$datetime = new DateTime($value);
        $datetime = DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['date_contacted'] = $datetime->format('Y-m-d');
    }

    public function getDateContactedAttribute($value){
        return Carbon::parse($value)->format('m-d-Y');
    }

    public function setDateInspectedAttribute($value){
        //$datetime = new DateTime($value);
        $datetime = DateTime::createFromFormat('m-d-Y', $value);
        return $this->attributes['date_inspected'] = $datetime->format('Y-m-d');
    }

    public function getDateInspectedAttribute($value){
        return Carbon::parse($value)->format('m-d-Y');
    }
}
