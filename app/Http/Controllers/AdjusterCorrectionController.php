<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAdjusterCorrection;
use App\Models\AdjusterCorrection;
use App\Models\Carrier;
use App\Models\Leader;
use App\Models\MasterAdjuster;
use App\Models\Qa;
use Illuminate\Database\Eloquent\Builder;

class AdjusterCorrectionController extends Controller
{
    public function index(){
        $adj_corrections = AdjusterCorrection::with('field_adjuster')->get();

        return view('adjuster-correction.index', compact('adj_corrections'));
    }

    public function create(){
        $carriers = Carrier::all();
        $adjusters = MasterAdjuster::whereHas('types', function(Builder $query){
            $query->where('adjuster_type_id', 1);
        })->get();
        $qas = Qa::all();
        $leaders = Leader::all();

        return view('adjuster-correction.create', compact('carriers', 'adjusters', 'qas', 'leaders'));
    }

    public function store(StoreAdjusterCorrection $request){
        //getting only the validated fields
        $data = $request->validated();
        AdjusterCorrection::create([
              "review_date" => $data['review_date'],
              "catastrophe_claim" => ($data['catastrophe_claim'] == 'yes') ? 1 : 0,
              "claim_number" => $data['claim_number'],
              'carrier_id' => $data['carrier'],
              "insd_last_name" => $data['insd_last_name'],
              "master_adjuster_id" => $data['adjuster_id'],
              "qa_id" => $data['qa'],
              "leader_id" => $data['leader'],
              "fa_glr_correction" => $data['fa_glr_correction'],
              "fa_est_correction" => $data['fa_est_correction'],
              "fa_photo_correction" => $data['fa_photo_correction'],
              "fa_total_correction" => $data['fa_total_correction'],
              "qa_glr_correction" => $data['qa_glr_correction'],
              "qa_est_correction" => $data['qa_est_correction'],
              "qa_photo_correction" => $data['qa_photo_correction'],
              "qa_total_correction" => $data['qa_tot_correction'],
              "correction_detail" => $data['correction_details']
        ]);

        return redirect()->route('adjuster-corrections.index')->with([
           'status' => 'success',
           'message' => 'Correction has been added.'
        ]);
    }

    public function show($id){
        $adj_correction = AdjusterCorrection::findOrFail($id);

        return view('adjuster-correction.show', compact('adj_correction'));
    }

    public function edit($id){
        $adj_correction = AdjusterCorrection::findOrFail($id);
        $carriers = Carrier::all();
        $adjusters = MasterAdjuster::whereHas('types', function(Builder $query){
            $query->where('adjuster_type_id', 1);
        })->get();
        $qas = Qa::all();
        $leaders = Leader::all();


        return view('adjuster-correction.edit', compact('adj_correction', 'carriers', 'adjusters', 'qas', 'leaders'));
    }

    public function update(StoreAdjusterCorrection $request, $id){
        //getting only the validated fields
        $data = $request->validated();
        $adj_correction = AdjusterCorrection::findOrFail($id);
        $adj_correction->update([
            "review_date" => $data['review_date'],
            "catastrophe_claim" => ($data['catastrophe_claim'] == 'yes') ? 1 : 0,
            "claim_number" => $data['claim_number'],
            'carrier_id' => $data['carrier'],
            "insd_last_name" => $data['insd_last_name'],
            "master_adjuster_id" => $data['adjuster_id'],
            "qa_id" => $data['qa'],
            "leader_id" => $data['leader'],
            "fa_glr_correction" => $data['fa_glr_correction'],
            "fa_est_correction" => $data['fa_est_correction'],
            "fa_photo_correction" => $data['fa_photo_correction'],
            "fa_total_correction" => $data['fa_total_correction'],
            "qa_glr_correction" => $data['qa_glr_correction'],
            "qa_est_correction" => $data['qa_est_correction'],
            "qa_photo_correction" => $data['qa_photo_correction'],
            "qa_total_correction" => $data['qa_tot_correction'],
            "correction_detail" => $data['correction_details']
        ]);

        return back()->with([
           'status' => 'success',
           'message' => 'Correction has been updated.'
        ]);
    }

    public function destroy($id)
    {
        $city = AdjusterCorrection::findOrFail($id);
        $city->delete();

        return redirect()->route('adjuster-corrections.index')->with([
            'status' => 'success',
            'message' => 'Adjuster correction has been deleted successfully'
        ]);
    }
}
