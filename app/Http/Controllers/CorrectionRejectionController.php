<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCorrectionRejection;
use App\Models\AdjusterCorrection;
use App\Models\Carrier;
use App\Models\CorrectionRejection;
use App\Models\Leader;
use App\Models\MasterAdjuster;
use App\Models\Qa;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class CorrectionRejectionController extends Controller
{
    public function index(){
        $correction_rejections = CorrectionRejection::all();
//        $adjuster_rejections = AdjusterCorrection::all();

        return view('correction-rejection.index', compact('correction_rejections'));
    }

    public function create(){
        $carriers = Carrier::all();
        $adjusters = MasterAdjuster::whereHas('types', function(Builder $query){
            $query->where('adjuster_type_id', 1);
        })->get();
        $qas = Qa::all();
        $leaders = Leader::all();


        return view('correction-rejection.create', compact('carriers', 'adjusters', 'qas', 'leaders'));
    }

    public function store(StoreCorrectionRejection $request){
        //get the validated fields
        $data = $request->all();
        CorrectionRejection::create([
            "review_date" => $data['review_date'],
            "catastrophe_claim" => ($data['catastrophe_claim'] == 'yes') ? 1 : 0,
            "claim_number" => $data['claim_number'],
            "insd_last_name" => $data['insd_last_name'],
            "adjuster_id" => $data['adjuster_id'],
            "carrier_id" => $data['carrier'],
            "qa_id" => $data['qa'],
            "leader_id" => $data['leader'],
            'field_glr_number_exc' => $data['fld_glr_number_exc'],
            'field_est_number_exc' => $data['fld_est_number_exc'],
            'field_total_number_exc' => $data['fld_total_number_exc'],
            'qa_glr_number_exc' => $data['qa_glr_number_exc'],
            'qa_est_number_exc' => $data['qa_est_number_exc'],
            'qa_photo_number_exc' => $data['qa_photo_number_exc'],
            'qa_total_number_exc' => $data['qa_total_number_exc'],
            'glr_cov_narrative' => $data['glr_cov_narrative'],
            'date_ctc_insp' => $data['date_ctc_insp'],
            'mortgage_co' => $data['mortgage_co'],
            'col_cause_of_loss' => $data['col_cause_of_loss'],
            'subro_salv_details' => $data['subro_salv_details'],
            'wind_hail_rpt' => $data['wind_hail_rpt'],
            'new_survey' => $data['new_survey'],
            'logo_exc' => $data['logo_exc'],
            'other_glr_exc' => $data['other_glr_exc'],
            'current_xact' => $data['current_xact'],
            'deductible_application' => $data['deductible_application'],
            'no_cov_dam_dele_part_all' => $data['no_cov_dam_dele_part_all'],
            'overhead_and_profit' => $data['overhead_and_profit'],
            'measure_accuracy' => $data['measure_accuracy'],
            'sketch' => $data['sketch'],
            'repair_vs_replace' => $data['repair_vs_replace'],
            'acv_vs_rcv' => $data['acv_vs_rcv'],
            'depreciation' => $data['depreciation'],
            'line_item_exc' => $data['line_item_exc'],
            'contents_exc' => $data['contents_exc'],
            'drywall' => $data['drywall'],
            'insulation' => $data['insulation'],
            'cleaning_mit' => $data['cleaning_mit'],
            'paint' => $data['paint'],
            'trim_and_base' => $data['trim_and_base'],
            'flooring' => $data['flooring'],
            'cabinets' => $data['cabinets'],
            'debris_removal' => $data['debris_removal'],
            'other_est_exc' => $data['other_est_exc'],
            'oth_structure_proper_items' => $data['oth_structure_proper_items'],
            'oth_str_acv_vs_rcv' => $data['oth_str_acv_vs_rcv'],
            'oth_str_other_exc' => $data['oth_str_other_exc'],
            'roof_wind_bp' => $data['roof_wind_bp'],
            'roof_hail_bp' => $data['roof_hail_bp'],
            'roof_valley_or_ridge' => $data['roof_valley_or_ridge'],
            'roof_repair_vs_replace' => $data['roof_repair_vs_replace'],
            'roof_meas_or_quantity' => $data['roof_meas_or_quantity'],
            'roof_miss_items' => $data['roof_miss_items'],
            'roof_add_labor' => $data['roof_add_labor'],
            'roof_other_exc' => $data['roof_other_exc'],
            'photo_bp' => $data['photo_bp'],
            'photo_clear' => $data['photo_clear'],
            'photo_all_damage' => $data['photo_all_damage'],
            'photo_label' => $data['photo_label'],
            'exception_details' => $data['exception_details'],
        ]);

        return redirect()->route('correction-rejections.index')->with([
           'status' => 'success',
           'message' => 'Correction has been added.'
        ]);
    }

    public function show($id){
        $correction_rejection = CorrectionRejection::findOrFail($id);
        return view('correction-rejection.show',compact('correction_rejection'));
    }

    public function edit($id){
        $correction_rejection = CorrectionRejection::findOrFail($id);
        $carriers = Carrier::all();
        $adjusters = MasterAdjuster::whereHas('types', function(Builder $query){
            $query->where('adjuster_type_id', 1);
        })->get();
        $qas = Qa::all();
        $leaders = Leader::all();

        return view('correction-rejection.edit', compact('correction_rejection', 'carriers', 'adjusters', 'qas', 'leaders'));
    }

    public function update(StoreCorrectionRejection $request, $id){
        //get the validated fields
        $data = $request->all();

        $correction_rejection = CorrectionRejection::findOrFail($id);
        $correction_rejection->update([
            "review_date" => $data['review_date'],
            "catastrophe_claim" => ($data['catastrophe_claim'] == 'yes') ? 1 : 0,
            "claim_number" => $data['claim_number'],
            "insd_last_name" => $data['insd_last_name'],
            "adjuster_id" => $data['adjuster_id'],
            "qa_id" => $data['qa'],
            "carrier_id" => $data['carrier'],
            "leader_id" => $data['leader'],
            'field_glr_number_exc' => $data['fld_glr_number_exc'],
            'field_est_number_exc' => $data['fld_est_number_exc'],
            'field_photo_number_exc' => $data['fld_photo_number_exc'],
            'field_total_number_exc' => $data['fld_total_number_exc'],
            'qa_glr_number_exc' => $data['qa_glr_number_exc'],
            'qa_est_number_exc' => $data['qa_est_number_exc'],
            'qa_photo_number_exc' => $data['qa_photo_number_exc'],
            'qa_total_number_exc' => $data['qa_total_number_exc'],
            'glr_cov_narrative' => $data['glr_cov_narrative'],
            'date_ctc_insp' => $data['date_ctc_insp'],
            'mortgage_co' => $data['mortgage_co'],
            'col_cause_of_loss' => $data['col_cause_of_loss'],
            'subro_salv_details' => $data['subro_salv_details'],
            'wind_hail_rpt' => $data['wind_hail_rpt'],
            'new_survey' => $data['new_survey'],
            'logo_exc' => $data['logo_exc'],
            'other_glr_exc' => $data['other_glr_exc'],
            'current_xact' => $data['current_xact'],
            'deductible_application' => $data['deductible_application'],
            'no_cov_dam_dele_part_all' => $data['no_cov_dam_dele_part_all'],
            'overhead_and_profit' => $data['overhead_and_profit'],
            'measure_accuracy' => $data['measure_accuracy'],
            'sketch' => $data['sketch'],
            'repair_vs_replace' => $data['repair_vs_replace'],
            'acv_vs_rcv' => $data['acv_vs_rcv'],
            'depreciation' => $data['depreciation'],
            'line_item_exc' => $data['line_item_exc'],
            'contents_exc' => $data['contents_exc'],
            'drywall' => $data['drywall'],
            'insulation' => $data['insulation'],
            'cleaning_mit' => $data['cleaning_mit'],
            'paint' => $data['paint'],
            'trim_and_base' => $data['trim_and_base'],
            'flooring' => $data['flooring'],
            'cabinets' => $data['cabinets'],
            'debris_removal' => $data['debris_removal'],
            'other_est_exc' => $data['other_est_exc'],
            'oth_structure_proper_items' => $data['oth_structure_proper_items'],
            'oth_str_acv_vs_rcv' => $data['oth_str_acv_vs_rcv'],
            'oth_str_other_exc' => $data['oth_str_other_exc'],
            'roof_wind_bp' => $data['roof_wind_bp'],
            'roof_hail_bp' => $data['roof_hail_bp'],
            'roof_valley_or_ridge' => $data['roof_valley_or_ridge'],
            'roof_repair_vs_replace' => $data['roof_repair_vs_replace'],
            'roof_meas_or_quantity' => $data['roof_meas_or_quantity'],
            'roof_miss_items' => $data['roof_miss_items'],
            'roof_add_labor' => $data['roof_add_labor'],
            'roof_other_exc' => $data['roof_other_exc'],
            'photo_bp' => $data['photo_bp'],
            'photo_clear' => $data['photo_clear'],
            'photo_all_damage' => $data['photo_all_damage'],
            'photo_label' => $data['photo_label'],
            'exception_details' => $data['exception_details'],
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Correction has been updated.'
        ]);
    }

    public function destroy($id){
        $correction_rejection = CorrectionRejection::findOrFail($id);
        $correction_rejection->delete();

        return redirect()->route('correction-rejections.index')->with([
            'status' => 'success',
            'message' => 'Correction has been deleted.'
        ]);
    }
}
