<?php

namespace App\Http\Controllers;

use App\Models\AdjusterStateLicense;
use App\Models\City;
use App\Models\MasterAdjuster;
use App\Models\State;
use App\Models\StateServiceArea;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;


class AdjusterHomeStateLicenseController extends Controller
{
    public function index(){
        $adjusters = MasterAdjuster::where('active', 1)->idDescending()->get();

        return view('adjuster-state-license.index', compact('adjusters'));
    }

    public function create(){
        $states = State::all();
        $adjusters = MasterAdjuster::whereHas('types', function(Builder $query){
            $query->where('adjuster_type_id', 1);
        })->get();

        return view('adjuster-state-license.create',compact('states', 'adjusters'));
    }

    public function store(Request $request){
        if($request->has('home_state')){
            //first update the previous home state license
            AdjusterStateLicense::where('master_adjuster_id', $request->adjuster)
                ->where('home_state', 1)
                ->update([
                    'home_state' => 0
                ]);
        }

        AdjusterStateLicense::create([
            'master_adjuster_id' => $request->adjuster,
            'state_id' => $request->license_state,
            'license_number' => $request->license_number,
            'expiry_date' => $request->expiry_date,
            'npn_number' => $request->npn_number,
            'home_state' => ($request->has('home_state')) ? 1 : 0,
            'active' => $request->active,
            'comment' => $request->comment
        ]);

        return redirect()->route('home-state-license.index')->with([
            'status' => 'success',
            'message' => 'State license has been added.'
        ]);
    }

    public function edit($id){
        $adjuster = MasterAdjuster::with('home_state_licenses')->where('id', $id)->first();
        $states = State::all();
        $cities = City::all();

        return view('adjuster-state-license.edit', compact('adjuster', 'states', 'cities'));
    }

    public function show($id){
        $adjuster = MasterAdjuster::findOrFail($id);

        return view('adjuster-state-license.show', compact('adjuster'));
    }

    public function update(Request $request, $id){
//        dd($request->all());
        $license = AdjusterStateLicense::findOrFail($id);

        if($request->has('home_state')){
            //first update the previous home state license
            AdjusterStateLicense::where('master_adjuster_id', $request->adjuster_id)
                ->where('home_state', 1)
                ->update([
                    'home_state' => 0
                ]);
        }

        $license->update([
            'state_id' => $request->license_state,
            'license_number' => $request->license_number,
            'expiry_date' => $request->expiry_date,
            'npn_number' => $request->npn_number,
            'home_state' => ($request->has('home_state')) ? 1 : 0,
            'active' => $request->active,
            'comment' => $request->comment
        ]);

        return redirect()->route('home-state-license.edit', $request->adjuster_id)->with([
            'status' => 'success',
            'message' => 'State license has been updated.'
        ]);
    }

    public function destroy($id){
        $adjuster = MasterAdjuster::findOrFail($id);
        $adjuster->home_state_licenses()->delete();

        return redirect()->route('home-state-license.index')->with([
            'status' => 'success',
            'message' => 'State license has been deleted.'
        ]);
    }

    public function editSingle($id){
        $license = AdjusterStateLicense::findOrFail($id);
        $states = State::all();
        $cities = City::all();
        return view('adjuster-state-license.edit-single', compact('license', 'states', 'cities'));
    }
}

