<?php

namespace App\Http\Controllers;

use App\Models\StateServiceAreaT1;
use Illuminate\Http\Request;

class ServiceStateAreaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $service_state_areas = StateServiceAreaT1::all();

        return view('supporting-forms.service-state-area.index', compact('service_state_areas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supporting-forms.service-state-area.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'name' => 'required|string'
        ]);

        StateServiceAreaT1::create([
           'name' => $request->name
        ]);

        return redirect()->route('service-state-area.index')->with([
           'status' => 'success',
           'message' => 'Service State Area has been added.'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service_state_area = StateServiceAreaT1::findOrFail($id);

        return view('supporting-forms.service-state-area.edit', compact('service_state_area'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);
        $service_state_area = StateServiceAreaT1::findOrFail($id);
        $service_state_area->update([
           'name' => $request->name
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Service State Area has been updated.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service_state_area = StateServiceAreaT1::findOrFail($id);
        $service_state_area->delete();

        return redirect()->route('service-state-area.index')->with([
            'status' => 'success',
            'message' => 'Service State Area has been deleted.'
        ]);
    }
}
