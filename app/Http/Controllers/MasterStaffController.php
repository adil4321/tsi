<?php

namespace App\Http\Controllers;

use App\Models\AssignRole;
use App\Models\MasterAdjuster;
use App\Models\PersonalInactiveReason;
use App\Models\AdjusterGrade;
use App\Models\StateServiceArea;
use App\Models\StateServiceAreaT1;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class MasterStaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $staff_adjusters = MasterAdjuster::where('adjuster_type', 'staff')->orderBy('id', 'desc')->get();

        return view('master-staff.index', compact('staff_adjusters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assign_roles = AssignRole::all();
        $ranks = AdjusterGrade::all();
        $state_service_areas = StateServiceArea::all();
        $personal_inactive_reasons = PersonalInactiveReason::all();
        $state_service_areas_t1 = StateServiceAreaT1::all();

        return view('master-staff.create', compact('assign_roles', 'ranks', 'state_service_areas', 'state_service_areas_t1', 'personal_inactive_reasons'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function edit($id){
        $staff_adjuster = MasterAdjuster::findOrFail($id);
        $assign_types = AssignRole::all();
        $ranks = AdjusterGrade::all();
        $state_service_areas = StateServiceArea::all();
        $personal_inactive_reasons = PersonalInactiveReason::all();
        $state_service_areas_t1 = StateServiceAreaT1::all();

        return view('master-staff.edit', compact('staff_adjuster','assign_types', 'ranks', 'state_service_areas', 'state_service_areas_t1', 'personal_inactive_reasons'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        $this->Validator($request->all())->validate();
        MasterAdjuster::create([
            'adjuster_type' => 'staff',
            'initial_service_date' => $request->initial_service_date,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'dob' => $request->dob,
            'email' => $request->email,
            'primary_phone_no' => $request->primary_phone_number,
            'secondary_phone_no' => $request->secondary_phone_number,
            'emergency_contact_name' => $request->emergency_contact_name,
            'emergency_phone_no' => $request->emergency_contact_phone,
            'street_number' => $request->street_number_name,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
            'driver_license' => $request->driver_license,
            'dl_state' => $request->dl_state,
            'xact_address' => $request->exact_address,
            'active' => $request->active,
            'current_active_state' => json_encode($request->current_active_state),
            'personal_inactive_reason_id' => $request->inactive_rsn,
            'tx_id' => $request->tax_idds,
            'residence_license_state' => $request->residence_lic_st,
            'residence_license_number' => $request->residence_lic_num,
            'license_expiry_date' => $request->lic_exp_dt,
            'state_license' => $request->states_licensed_fl,
            'speciality' => $request->specialty_large_content,
            'personal_folder_made' => $request->personnel_folder_made,
            'fluent_languages' => $request->initials_of_fluent_languages,
            'comment' => $request->comment,
            'staff_role_id' => $request->staff_role,
            'npn_number' => $request->npn_number,
            'check_background_date' => $request->date_background_check_completed
        ]);

        return redirect()->route('staff-adjusters.index')->with([
            'status' => 'success',
            'message' => 'Staff has been created successfully'
        ]);
    }

    public function update(Request $request, $id){
        $this->Validator($request->all(), $id)->validate();
        $staff_adjuster = MasterAdjuster::findOrFail($id);
        $staff_adjuster->update([
            'adjuster_type' => 'staff',
            'initial_service_date' => $request->initial_service_date,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'dob' => $request->dob,
            'email' => $request->email,
            'primary_phone_no' => $request->primary_phone_number,
            'secondary_phone_no' => $request->secondary_phone_number,
            'emergency_contact_name' => $request->emergency_contact_name,
            'emergency_phone_no' => $request->emergency_contact_phone,
            'street_number' => $request->street_number_name,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
            'driver_license' => $request->driver_license,
            'dl_state' => $request->dl_state,
            'xact_address' => $request->exact_address,
            'active' => $request->active,
            'current_active_state' => json_encode($request->current_active_state),
            'personal_inactive_reason_id' => $request->inactive_rsn,
            'tx_id' => $request->tax_idds,
            'residence_license_state' => $request->residence_lic_st,
            'residence_license_number' => $request->residence_lic_num,
            'license_expiry_date' => $request->lic_exp_dt,
            'state_license' => $request->states_licensed_fl,
            'speciality' => $request->specialty_large_content,
            'personal_folder_made' => $request->personnel_folder_made,
            'fluent_languages' => $request->initials_of_fluent_languages,
            'comment' => $request->comment,
            'staff_role_id' => $request->staff_role,
            'npn_number' => $request->npn_number,
            'check_background_date' => $request->date_background_check_completed
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Staff Details has been updated.'
        ]);
    }

    public function Validator(array $data, $id = null){
        return Validator::make($data, [
            "first_name" => "required|min:3|string",
            "last_name" => "required|min:3|string",
            "email" => "required|email|unique:master_adjusters,email,".$id ?? '',
            "primary_phone_number" => "required|string",
            "secondary_phone_number" => "string",
            "emergency_contact_name" => "string",
            "emergency_contact_phone" => "string",
            "dob" => "required|string",
            "driver_license" => "required|string",
            "street_number_name" => "required|string",
            "city" => "required|string",
            "state" => "required|string",
            "zip" => "string",
            "dl_state" => "string",
            "tax_idds" => "required|string",
            "initials_of_fluent_languages" => "nullable|string",
            "lic_exp_dt" => "required|string",
            "residence_lic_st" => "string",
            "residence_lic_num" => "required|string",
            "current_active_state.*" => "required|string",
            "personnel_folder_name" => 'string',
            "active" => "required|integer",
            "states_licensed_fl" => "required|string",
            "specialty_large_content" => "nullable|string",
            'inactive_rsn' => "required|integer|exists:personal_inactive_reasons,id",
            "initial_service_date" => "required|string",
            "staff_role" => 'nullable|string',
            'npn_number' => 'required|string',
            'date_background_check_completed' => 'required|string',
            'comment' => 'nullable'
        ]);
    }
}
