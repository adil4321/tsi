<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\UpdateMasterAdjusterRequest;
use App\Models\AdjusterCertificate;
use App\Models\AdjusterStateLicense;
use App\Models\AdjusterTypes;
use App\Models\AssignRole;
use App\Models\City;
use App\Models\GradeReason;
use App\Models\Language;
use App\Models\MasterAdjuster;
use App\Models\PersonalInactiveReason;
use App\Models\AdjusterGrade;
use App\Models\Speciality;
use App\Models\StateServiceAreaT1;
use App\Models\FieldAccess;
use App\Models\PersonalStatus;
use App\Models\State;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Session;

class MasterAdjusterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('adjuster_master_slug') == "Total Adjusters"){
        $adjusters = MasterAdjuster::with('log_assignments')->orderBy('id', 'desc')->get();
        Session::put('adjuster_master_slug',null);    
    }
    else if(Session::get('adjuster_master_slug') == "Total Field Adjusters"){
        $adjusters = MasterAdjuster::with('log_assignments')->whereHas('types', function(Builder $builder){
            $builder->where('adjuster_type_id', 1);
        })->orderBy('id', 'desc')->get();
        Session::put('adjuster_master_slug',null);
    }
    else if(Session::get('adjuster_master_slug') == "Suspended Or Not Used"){
        $adjusters = MasterAdjuster::with('log_assignments')->where('active', 0)->orderBy('id', 'desc')->get();
        Session::put('adjuster_master_slug',null);
    }
    else if(Session::get('adjuster_master_slug') == "Total Staff Adjusters"){
        $adjusters = MasterAdjuster::with('log_assignments')->whereHas('types', function(Builder $builder){
            $builder->where('adjuster_type_id', 2);
        })->orderBy('id', 'desc')->get();
        Session::put('adjuster_master_slug',null);
    }
    else if(Session::get('adjuster_master_slug') == "Adjuster Have No State License"){
        $adjusters = MasterAdjuster::with('log_assignments')->doesntHave('home_state_licenses')->orderBy('id', 'desc')->get();
        Session::put('adjuster_master_slug',null);
    }
    else if(Session::get('adjuster_master_slug') == "Out Of Office Adjusters"){
        $adjusters = [];
        Session::put('adjuster_master_slug',null);
    }
        else{
            $adjusters = MasterAdjuster::with(['log_assignments' => function($query){
                $query->where('status', 1);
            }])->orderBy('id', 'desc')->get();
            Session::put('adjuster_master_slug',null);
        }
        $form_fields = FieldAccess::first();
        return view('master-adjuster.index', compact('adjusters','form_fields'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assign_roles = AssignRole::all();
        $adjuster_types = AdjusterTypes::all();
        $adjuster_grades = AdjusterGrade::all();
        $grade_reasons = GradeReason::all();
        $states = State::all();
        $personal_inactive_reasons = PersonalInactiveReason::all();
        $state_service_areas_t1 = StateServiceAreaT1::all();
        $languages = Language::all();
        $specialities = Speciality::all();
        $personal_statuses = PersonalStatus::all();

        return view('master-adjuster.create', compact('assign_roles', 'adjuster_types', 'adjuster_grades',
                          'grade_reasons', 'languages','states', 'state_service_areas_t1', 'personal_inactive_reasons',
                          'specialities', 'personal_statuses'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return $request->inactive_start_date;
        $this->Validator($request->all())->validate();

        //username
        $user_name = $request->first_name.' '.$request->last_name;
        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'user_name' => Str::slug($user_name),
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role_id' => 2
        ]);
        $master_adjuster = MasterAdjuster::create([
            'user_id' => $user->id,
            'initial_service_date' => $request->initial_service_date,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'dob' => $request->dob,
            'email' => $request->email,
            'primary_phone_no' => $request->primary_phone_number,
            'secondary_phone_no' => $request->secondary_phone_number,
            'emergency_contact_name' => $request->emergency_contact_name,
            'emergency_phone_no' => $request->emergency_contact_phone,
            'street_number' => $request->street_number_name,
            'city_id' => $request->city,
            'state_id' => $request->state_id,
            'zip' => $request->zip,
            'assign_role_id' => $request->assign_role,
            'driver_license' => $request->driver_license,
            'dl_state' => $request->dl_state,
            'xact_address' => $request->exact_address,
            //'state_service_area_id' => $request->primary_serv_state,
            'state_service_area_t1_id' => $request->service_area_of_state,
            'current_active_state' => ($request->has('current_active_state')) ? json_encode($request->current_active_state) : null,
            'active' => 1,
            'personal_inactive_reason_id' => $request->inactive_rsn,
            'tx_id' => $request->tax_idds,
            'company_llc_name' => $request->company_llc_name,
            'company_tin' => $request->company_tin,
            'state_license' => $request->states_licensed_fl,
            'npn_number' => $request->npn_number,
            'speciality' => ($request->has('specialities')) ? json_encode($request->specialities): null,
            'personal_folder_made' => $request->personnel_folder_made,
            'fluent_languages' => ($request->has('fluent_languages')) ? json_encode($request->fluent_languages) : null,
            'comment' => $request->comment,
            'url' => $request->url,
            'grade_reason_id' => $request->grade_reason,
            'adjuster_grade_id' => $request->adjuster_grade,
            'check_background_date' => $request->check_background_date,
            'is_flagged' => ($request->flagged) ? $request->flagged : 0,
            'inactive_end_date' => $request->inactive_end_date,
            'inactive_start_date' => $request->inactive_start_date,
            'flag_check_date' => $request->flag_check_date
        ]);

        $master_adjuster->types()->sync($request->adjuster_types);

        if($request->has('certificate_names')){
            foreach ($request->certificate_names as $key => $certificate_name){
                AdjusterCertificate::create([
                    'master_adjuster_id' => $master_adjuster->id,
                    'name' => $certificate_name,
                    'expiry_date' => $request->certificate_expiry_dates[$key]
                ]);
            }
        }

        if($request->has('license_states')){
            foreach ($request->license_states as $key => $state){
                AdjusterStateLicense::create([
                    'master_adjuster_id' => $master_adjuster->id,
                    'state_id' => $state,
                    'license_number' => $request->license_numbers[$key],
                    'comment' => $request->license_comments[$key],
                    'expiry_date' => $request->license_expiry_dates[$key],
                    // 'npn_number' => $request->license_npn_numbers[$key],
                    'home_state' => ($request->state == $state) ? 1 : 0
                ]);
            }
        }

        return redirect()->route('adjusters.index')->with([
            'status' => 'success',
            'message' => 'Adjuster has been created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $adjuster = MasterAdjuster::findOrFail($id);
        return view('master-adjuster.show', compact('adjuster'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $adjuster = MasterAdjuster::findOrFail($id);
        $assign_roles = AssignRole::all();
        $adjuster_types = AdjusterTypes::all();
        $adjuster_grades = AdjusterGrade::all();
        $grade_reasons = GradeReason::all();
        $states = State::all();
        $cities = City::where('state_id', $adjuster->state_id)->get();
        $personal_inactive_reasons = PersonalInactiveReason::all();
        $state_service_areas_t1 = StateServiceAreaT1::all();
        $languages = Language::all();
        $specialities = Speciality::all();
        $personal_statuses = PersonalStatus::all();

        return view('master-adjuster.edit', compact('adjuster','assign_roles', 'adjuster_types',
                          'adjuster_grades', 'grade_reasons', 'languages', 'states', 'state_service_areas_t1',
                          'personal_inactive_reasons', 'specialities', 'cities', 'personal_statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $requeste
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMasterAdjusterRequest $request, $id)
    {
        $adjuster = MasterAdjuster::findOrFail($id);
        $adjuster->update([
            'initial_service_date' => $request->initial_service_date,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'dob' => $request->dob,
            'email' => $request->email,
            'primary_phone_no' => $request->primary_phone_number,
            'secondary_phone_no' => $request->secondary_phone_number,
            'emergency_contact_name' => $request->emergency_contact_name,
            'emergency_phone_no' => $request->emergency_contact_phone,
            'street_number' => $request->street_number_name,
            'city_id' => $request->city,
            'state_id' => $request->state_id,
            'zip' => $request->zip,
            'assign_role_id' => $request->assign_role,
            'driver_license' => $request->driver_license,
            'dl_state' => $request->dl_state,
            'xact_address' => $request->exact_address,
            'state_service_area_t1_id' => $request->service_area_of_state,
            'current_active_state' => ($request->has('current_active_state')) ? json_encode($request->current_active_state) : null,
            'active' => 1,
            'personal_inactive_reason_id' => $request->inactive_rsn,
            'tx_id' => $request->tax_idds,
            'company_llc_name' => $request->company_llc_name,
            'company_tin' => $request->company_tin,
            'state_license' => $request->states_licensed_fl,
            'npn_number' => $request->npn_number,
            'speciality' => ($request->has('specialities')) ? json_encode($request->specialities): null,
            'personal_folder_made' => $request->personnel_folder_made,
            'fluent_languages' => ($request->has('fluent_languages')) ? json_encode($request->fluent_languages) : null,
            'comment' => $request->comment,
            'url' => $request->url,
            'grade_reason_id' => $request->grade_reason,
            'adjuster_grade_id' => $request->adjuster_grade,
            'check_background_date' => $request->date_background_check_complete,
            'is_flagged' => ($request->flagged) ? $request->flagged : 0,
            'inactive_start_date' => $request->inactive_start_date,
            'inactive_end_date' => $request->inactive_end_date,
            'flag_check_date' => $request->flag_check_date
        ]);

        $adjuster->types()->sync($request->adjuster_types);

        //deleting the previous certificates
        AdjusterCertificate::where('master_adjuster_id', $adjuster->id)->delete();
        //deleting the previous state license
        AdjusterStateLicense::where('master_adjuster_id', $adjuster->id)->delete();

        if($request->has('certificate_names')){
            foreach ($request->certificate_names as $key => $certificate_name){
                AdjusterCertificate::create([
                    'master_adjuster_id' => $adjuster->id,
                    'name' => $certificate_name,
                    'expiry_date' => $request->certificate_expiry_dates[$key]
                ]);
            }
        }

        if($request->has('license_states')){
            foreach ($request->license_states as $key => $state){
                AdjusterStateLicense::create([
                    'master_adjuster_id' => $adjuster->id,
                    'state_id' => $state,
                    'license_number' => $request->license_numbers[$key],
                    'comment' => $request->license_comments[$key],
                    'expiry_date' => $request->license_expiry_dates[$key],
                    // 'npn_number' => $request->license_npn_numbers[$key],
                    'home_state' => ($request->state == $state) ? 1 : 0
                ]);
            }
        }

        return back()->with([
           'status' => 'success',
           'message' => 'Adjuster has been updated.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function Validator(array $data, $id = null){
        return Validator::make($data, [
            "adjuster_types.*" => "required|integer|exists:adjuster_types,id",
            "adjuster_grade" => "nullable|integer|exists:adjuster_grades,id",
            "grade_reason" => "nullable|integer|exists:grade_reasons,id",
            "certificates" => 'nullable',
            "certificates.*" => "string",
            "certificates_expiry_dates" => "nullable",
            "certificates_expiry_dates.*" => "string",
            "license_states" => "nullable",
            "license_states.*" => "integer|exists:state_service_areas,id",
            "license_numbers" => "nullable",
            "license_numbers.*" => "string",
            "license_expiry_dates.*" => 'string',
            // "license_npn_number.*" => 'string',
            "first_name" => "required|min:3|string",
            "last_name" => "required|min:3|string",
            "email" => "required|email|unique:master_adjusters,email,".$id ?? '',
            "password" => "required|string|min:6|confirmed",
            "primary_phone_number" => "required|string",
            "secondary_phone_number" => "nullable|string",
            "emergency_contact_name" => "nullable|string",
            "emergency_contact_phone" => "nullable|string",
            "dob" => "nullable|string",
            "driver_license" => "nullable|string",
            "street_number_name" => "nullable|string",
            "city" => "nullable|exists:cities,id",
            "state_id" => "nullable|integer|exists:states,id",
            "zip" => "nullable|string",
            "dl_state" => "nullable|string",
            "company_llc_name" => "nullable|string",
            "company_tin" => "nullable|string",
            "tax_idds" => "nullable|string",
            "fluent_languages" => "nullable",
            "fluent_languages.*" => "string",
            "exact_address" => "nullable|string",
            "service_area_of_state" => "integer",
            "personnel_folder_made" => 'nullable|string',
            "assign_role" => "nullable|exists:assign_roles,id",
            "specialties.*" => "string",
            'inactive_rsn' => "nullable|integer|exists:personal_inactive_reasons,id",
            "initial_service_date" => "nullable|string",
            "url" => "nullable|string",
            "flagged" => "nullable|integer",
            'inactive_start_date' => 'nullable',
            'inactive_end_date' => 'nullable',
            "flag_check_date" => "nullable|string"
        ]);
    }
}
