<?php

namespace App\Http\Controllers;

use App\Http\Requests\ClaimStatusRequest;
use App\Models\ClaimStatus;
use App\Models\MasterAdjuster;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ClaimStatusController extends Controller
{
    public function index(){
        $claim_statuses = ClaimStatus::all();

        return view('claim-status.index', compact('claim_statuses'));
    }

    public function create(){
        $adjusters = MasterAdjuster::whereHas('types', function(Builder $query){
            $query->where('adjuster_type_id', 1);
        })->get();

        return view('claim-status.create', compact('adjusters'));
    }

    public function store(ClaimStatusRequest $request){
        ClaimStatus::create([
            'tsi_number' => $request->tsi_number,
            'date_received' => $request->date_received,
            'master_adjuster_id' => $request->adjuster,
            'insured' => $request->insured,
            'company' => $request->company,
            'claim_number' => $request->claim_number,
            'date_of_notice' => $request->date_of_notice,
            'date_contacted' => $request->date_contacted,
            'date_inspected' => $request->date_inspected,
            'report_due' => $request->report_due,
            'note' => $request->note
        ]);

        return redirect()->route('claim-status.index')->with([
            'status' => 'success',
            'message' => 'Claim Status has been added.'
        ]);
    }

    public function show($id){
        $claim_status = ClaimStatus::findOrFail($id);

        return view('claim-status.show', compact('claim_status'));
    }

    public function edit($id){
        $claim_status = ClaimStatus::findOrFail($id);
        $adjusters = MasterAdjuster::whereHas('types', function(Builder $query){
            $query->where('adjuster_type_id', 1);
        })->get();

        return view('claim-status.edit', compact('claim_status', 'adjusters'));
    }

    public function update(ClaimStatusRequest $request, $id){
        $claim_status = ClaimStatus::findOrFail($id);
        $claim_status->update([
            'tsi_number' => $request->tsi_number,
            'date_received' => $request->date_received,
            'master_adjuster_id' => $request->adjuster,
            'insured' => $request->insured,
            'company' => $request->company,
            'claim_number' => $request->claim_number,
            'date_of_notice' => $request->date_of_notice,
            'date_contacted' => $request->date_contacted,
            'date_inspected' => $request->date_inspected,
            'report_due' => $request->report_due,
            'note' => $request->note
        ]);

        return redirect()->back()->with([
            'status' => 'success',
            'message' => 'Claim Status has been updated.'
        ]);
    }

    public function destroy($id){
        $claim_status = ClaimStatus::findOrFail($id);
        $claim_status->delete();

        return redirect()->route('claim-status.index')->with([
            'status' => 'success',
            'message' => 'Claim Status has been updated.'
        ]);
    }
}
