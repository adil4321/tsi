<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddLogAssignmentRequest;
use App\Http\Requests\StoreLogAssignment;
use App\Models\Carrier;
use App\Models\City;
use App\Models\ClAssignType;
use App\Models\CloseType;
use App\Models\LogFieldAccess;
use App\Models\FieldAssignSource;
use App\Models\LogAssignment;
use App\Models\MasterAdjuster;
use App\Models\State;
use App\Models\StateServiceArea;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class LogAssignmentController extends Controller
{
    public function index(){
        $log_assignments = LogAssignment::all();
        $form_fields =LogFieldAccess::first();

        return view('log-assignment.index', compact('log_assignments','form_fields'));
    }

    public function create(){
        $adjusters = MasterAdjuster::whereHas('types', function(Builder $query){
            $query->where('adjuster_type_id', 1);
        })->get();
        $states = State::all();

        return view('log-assignment.create', compact('adjusters', 'states'));
    }

    public function store(AddLogAssignmentRequest $request){
        LogAssignment::create([
              "claim_number" => $request['claim_number'],
              "tsi_file_number" => $request['tsi_file_number'],
              "master_adjuster_id" => $request['adjuster'],
              "insured" => $request['insured'],
              "state_id" => $request['state'],
              "city_id" => $request['city'],
              "zip_code" => $request['zip'],
              "date" => $request['date'],
              'status' => ($request->status == 'closed') ? 0 : 1
        ]);

        return redirect()->route('log-assignment.index')->with(['status'=> 'success', 'message' => 'Log Assignment has been added.']);
    }


    public function show($id){
        $log_assignment = LogAssignment::findOrFail($id);
        return view('log-assignment.show', compact('log_assignment'));
    }

    public function edit($id){
        $log_assignment = LogAssignment::findOrFail($id);
        $adjusters = MasterAdjuster::whereHas('types', function(Builder $query){
            $query->where('adjuster_type_id', 1);
        })->get();
        $states = State::all();
        $cities = City::all();

        return view('log-assignment.edit', compact('log_assignment','states','adjusters', 'cities'));
    }

    public function update(AddLogAssignmentRequest $request, $id){
        $log_assignment = LogAssignment::findOrFail($id);
        $log_assignment->update([
            "claim_number" => $request['claim_number'],
            "tsi_file_number" => $request['tsi_file_number'],
            "master_adjuster_id" => $request['adjuster'],
            "insured" => $request['insured'],
            "state_id" => $request['state'],
            "city_id" => $request['city'],
            "zip_code" => $request['zip'],
            "date" => $request['date'],
            'status' => ($request->status == 'closed') ? 0 : 1
        ]);

        return back()->with(['status'=> 'success', 'message' => 'Log Assignment has been updated.']);
    }

    public function delete($id){
        $log_assignment = LogAssignment::findOrFail($id);
        $log_assignment->delete();

        return redirect()->route('log-assignment.index')->with(['status'=> 'success', 'message' => 'Log Assignment has been deleted.']);
    }
}
