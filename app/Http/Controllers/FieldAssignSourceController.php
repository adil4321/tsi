<?php

namespace App\Http\Controllers;

use App\Models\FieldAssignSource;
use Illuminate\Http\Request;

class FieldAssignSourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $field_assign_sources = FieldAssignSource::orderBy('name', 'asc')->get();;

        return view('supporting-forms.field-assign-sources.index', compact('field_assign_sources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supporting-forms.field-assign-sources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);

        FieldAssignSource::create([
           'name' => $request->name
        ]);

        return redirect()->route('field-assign-sources.index')->with([
           'status' => 'success',
           'message' => 'Field assign source has been added'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $field_assign_source = FieldAssignSource::findOrFail($id);

        return view('supporting-forms.field-assign-sources.edit', compact('field_assign_source'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|string'
        ]);
        $source = FieldAssignSource::findOrFail($id);
        $source->update([
            'name' => $request->name
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Field assign source has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $source = FieldAssignSource::findOrFail($id);
        $source->delete();

        return redirect()->route('field-assign-sources.index')->with([
            'status' => 'success',
            'message' => 'Field assign source has been updated'
        ]);
    }
}
