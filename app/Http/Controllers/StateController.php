<?php

namespace App\Http\Controllers;

use App\Models\State;
use Illuminate\Http\Request;
use Session;

class StateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('adjuster_master_slug') == "Total States"){
            $states = State::orderBy('name', 'asc')->get();
            Session::put('adjuster_master_slug',null);
        }
        else{
            $states = State::orderBy('name', 'asc')->get();
        }
        
        return view('supporting-forms.states.index', compact('states'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('supporting-forms.states.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'name' => 'required|string',
           'short_name' => 'required|string'
        ]);

        State::create([
           'name' => $request->name,
           'short_name' => $request->short_name,
        ]);

        return redirect()->route('states.index')->with([
            'status' => 'success',
            'message' => 'State has been created'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $state = State::findOrFail($id);

        return view('supporting-forms.states.edit', compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'short_name' => 'required|string'
        ]);

        $state = State::findOrFail($id);

        $state->update([
            'name' => $request->name,
            'short_name' => $request->short_name,
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'State has been updated'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $state = State::findOrFail($id);
        $state->delete();

        return redirect()->route('states.index')->with([
            'status' => 'success',
            'message' => 'State has been deleted'
        ]);
    }
}
