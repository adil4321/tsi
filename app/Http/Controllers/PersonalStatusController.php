<?php

namespace App\Http\Controllers;

use App\Http\Requests\PersonalStatusRequest;
use App\Models\PersonalStatus;
use Illuminate\Http\Request;

class PersonalStatusController extends Controller
{
    public function index(){
        $personal_statuses = PersonalStatus::all();

        return view('supporting-forms.personal-status.index', compact('personal_statuses'));
    }

    public function create(){
        return view('supporting-forms.personal-status.create');
    }

    public function store(PersonalStatusRequest $request) {
        PersonalStatus::create([
            'name' => $request->name
        ]);
        
        return redirect()->route('personal-status.index')->with([
            'status' => 'success',
            'message' => 'Personal status has been created.'
        ]);
    }

    public function edit($id){
        $personal_status = PersonalStatus::findOrFail($id);
        return view('supporting-forms.personal-status.edit', compact('personal_status'));
    }

    public function update(PersonalStatusRequest $request, $id){
        $personal_status = PersonalStatus::findOrFail($id);
        $personal_status->update([
            'name' => $request->name
        ]);

        return back()->with([
            'status' => 'success',
            'message' => 'Personal status has been updated.'
        ]);
    }

    public function destroy($id){
        $personal_status = PersonalStatus::findOrFail($id);
        $personal_status->delete();
        return redirect()->route('personal-status.index')->with([
            'status' => 'success',
            'message' => 'Personal status has been deleted.'
        ]);
    }
}
