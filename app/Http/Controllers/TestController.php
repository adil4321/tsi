<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\StateServiceArea;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://www.universal-tutorial.com/api/getaccesstoken",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                // Set Here Your Requesred Headers
                'Accept: application/json',
                'api-token: QejYaWlrNDN4dtDp0s_yp0DOXtFAgMvc2EDLUX1Xe6Fjtsiwkq4KXJkTIVoGbg1SCkQ',
                'user-email: adilmughal350@gmail.com'
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            dd(json_decode($response));
        }
    }

    public function getCities(){
        $auth_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJfZW1haWwiOiJhZGlsbXVnaGFsMzUwQGdtYWlsLmNvbSIsImFwaV90b2tlbiI6IlFlallhV2xyTkRONGR0RHAwc195cDBET1h0RkFnTXZjMkVETFVYMVhlNkZqdHNpd2txNEtYSmtUSVZvR2JnMVNDa1EifSwiZXhwIjoxNTgzNDM2NDI2fQ.M61q9aEXo3zyLiPEZsWZjw37vr-BpsEM7YWZXpg0UaE';
        $states = StateServiceArea::where('id', '>', 49)->get();
        //dd($states);
        foreach($states as $state){
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://www.universal-tutorial.com/api/cities/".$state->name,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    // Set Here Your Requesred Headers
                    'Accept: application/json',
                    "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7InVzZXJfZW1haWwiOiJhZGlsbXVnaGFsMzUwQGdtYWlsLmNvbSIsImFwaV90b2tlbiI6IlFlallhV2xyTkRONGR0RHAwc195cDBET1h0RkFnTXZjMkVETFVYMVhlNkZqdHNpd2txNEtYSmtUSVZvR2JnMVNDa1EifSwiZXhwIjoxNTgzNDM2NDI2fQ.M61q9aEXo3zyLiPEZsWZjw37vr-BpsEM7YWZXpg0UaE"
                ),
            ));
            $response = curl_exec($curl);
            $err = curl_error($curl);
            curl_close($curl);
    
            if ($err) {
                echo "cURL Error #:" . $err;
            } else {
                $new_response = json_decode($response, true);
                
                foreach($new_response as $response){
                    City::create([
                        'state_id' => $state->id,
                        'name' => $response['city_name']
                    ]);
                }
            }
        }
        
    }
}
