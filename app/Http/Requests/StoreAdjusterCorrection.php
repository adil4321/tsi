<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdjusterCorrection extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              "review_date" => 'string',
              "catastrophe_claim" => 'string|required',
              "claim_number" => "string|required",
              "insd_last_name" => 'nullable|string',
              "carrier" => 'nullable|exists:carriers,id',
              "adjuster_id" => 'required|exists:master_adjusters,id',
              "qa" => 'required|exists:qas,id',
              "leader" => 'required|exists:leaders,id',
              "fa_glr_correction" => 'nullable|string',
              "fa_est_correction" => 'nullable|string',
              "fa_photo_correction" => 'nullable|string',
              "fa_total_correction" => 'nullable|string',
              "qa_glr_correction" => 'nullable|string',
              "qa_est_correction" => 'nullable|string',
              "qa_photo_correction" => 'nullable|string',
              "qa_tot_correction" => 'nullable|string',
              "correction_details" => 'nullable|string',
        ];
    }
}
