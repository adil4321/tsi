<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLogAssignment extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              "field_assign_source" => 'required|exists:field_assign_sources,id',
              "tsi_claim_number" => "nullable|string",
              "cl_assign_type" => "required|exists:cl_assign_types,id",
              "date_assign_rec" => "nullable|string",
              "assign_adjuster" => "required|exists:master_adjusters,id",
              "assigned" => "nullable",
              "insured" => "nullable|string",
              "insd_city" => "nullable|string",
              "state" => "nullable|exists:state_service_areas,id",
              "zip" => "nullable|string",
              "insd_phone" => "nullable|string",
              "carrier" => "nullable|exists:carriers,id",
              "co_claim_number" => "nullable|string",
              "cms" => "nullable",
              "acknowledgement_sent" => "nullable",
              "date_contacted" => "nullable|string",
              "date_inspected" => "nullable|string",
              "date_report_due" => "nullable|string",
              "date_rep_returned" => "nullable|string",
              "date_assign_closed" => "nullable|string",
              "close_type" => "nullable|exists:close_types,id",
              "date_reopened" => "nullable|string",
              "date_reclosed" => "nullable|string",
              "notes" => "nullable|string",
              "map_link" => 'nullable|string'
        ];
    }
}
