<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('field_accesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('user_id');
            $table->boolean('initial_service_date')->nullable();
            $table->boolean('adjuster_grade_id')->nullable();
            $table->boolean('grade_reason_id')->nullable();
            $table->boolean('dob')->nullable();
            $table->boolean('first_name')->nullable();
            $table->boolean('last_name')->nullable();
            $table->boolean('primary_phone_no')->nullable();
            $table->boolean('secondary_phone_no')->nullable();
            $table->boolean('emergency_contact_name')->nullable();
            $table->boolean('emergency_phone_no')->nullable();
            $table->boolean('email')->nullable();
            $table->boolean('street_number')->nullable();
            $table->boolean('city_id')->nullable();
            $table->boolean('state')->nullable();
            $table->boolean('zip')->nullable();
            $table->boolean('assign_type_id')->nullable();
            $table->boolean('driver_license')->nullable();
            $table->boolean('dl_state')->nullable();
            $table->boolean('company_llc_name')->nullable();
            $table->boolean('xact_address')->nullable();
            $table->boolean('state_service_area_id')->nullable();
            $table->boolean('state_service_area_t1_id')->nullable();
            $table->boolean('active');
            $table->boolean('current_active_state')->nullable();
            $table->boolean('personal_inactive_reason_id')->nullable();
            $table->boolean('tx_id')->nullable();
            $table->boolean('license_name')->nullable();
            $table->boolean('company_tin')->nullable();
            $table->boolean('state_license')->nullable();
            $table->boolean('speciality')->nullable();
            $table->boolean('personal_folder_made')->default(0);
            $table->boolean('fluent_languages')->nullable();
            $table->boolean('npn_number')->nullable();
            $table->boolean('check_background_date')->nullable();
            $table->boolean('is_flagged');
            $table->boolean('comment')->nullable();
            $table->boolean('url')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('field_accesses');
    }
}
