<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdColumnToMasterAdjustersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('master_adjusters', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->after('id');
            $table->boolean('is_flagged')->default(0)->after('check_background_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('master_adjusters', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
