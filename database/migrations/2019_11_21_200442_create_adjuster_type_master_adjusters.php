<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdjusterTypeMasterAdjusters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adjuster_type_master_adjusters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('master_adjuster_id')->unsigned();
            $table->integer('adjuster_type')->unsigned();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adjuster_type_master_adjusters');
    }
}
