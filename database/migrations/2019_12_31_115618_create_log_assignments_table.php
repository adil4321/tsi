<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_assignments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('master_adjuster_id');
            $table->string('tsi_file_number')->nullable();
            $table->string('claim_number')->nullable();
            $table->string('insured')->nullable();
            $table->unsignedInteger('city_id');
            $table->unsignedInteger('state_id');
            $table->string('zip_code')->nullable();
            $table->date('date')->nullable();
            $table->string('company')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_assignments');
    }
}
