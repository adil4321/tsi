<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClaimStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('claim_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tsi_number')->nullable();
            $table->string('date_received')->nullable();
            $table->unsignedBigInteger('master_adjuster_id');
            $table->string('insured')->nullable();
            $table->string('company')->nullable();
            $table->string('claim_number')->nullable();
            $table->date('date_of_notice')->nullable();
            $table->date('date_contacted')->nulable();
            $table->date('date_inspected')->nullable();
            $table->string('report_due')->nullable();
            $table->text('note')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('claim_status');
    }
}
