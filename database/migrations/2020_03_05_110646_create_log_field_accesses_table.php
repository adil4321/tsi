<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogFieldAccessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_field_accesses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('master_adjuster_id')->default(0);
            $table->boolean('tsi_file_number')->default(0);
            $table->boolean('claim_number')->default(0);
            $table->boolean('insured')->default(0);
            $table->boolean('city_id')->default(0);
            $table->boolean('state_id')->default(0);
            $table->boolean('zip_code')->default(0);
            $table->boolean('date')->default(0);
            $table->boolean('company')->default(0);
            $table->boolean('status');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_field_accesses');
    }
}
