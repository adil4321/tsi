<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/cache-clear', function(){
   // \Illuminate\Support\Facades\Artisan::call('key:generate');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:cache');
    \Illuminate\Support\Facades\Artisan::call('migrate');
});
Route::middleware(['auth'])->group(function(){
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/test-api', 'TestController@index');
    Route::get('/test-api/cities', 'TestController@getCities');

    Route::resource('/users', 'UserController');
    
    //Route for Master Adjuster

    Route::resource('/adjusters', 'MasterAdjusterController');
    
    Route::post('/adjustermasterslug', 'AjaxController@adjustermasterslug')->name('adjustermasterslug');
    Route::get('/master-fields', 'MasterFieldAccessController@masterfields')->name('masterfields');
    Route::post('/master-fields/store', 'MasterFieldAccessController@store')->name('store');    
    //Routes for Personal Reasons
    Route::resource('/inactive-reasons', 'PersonalInactiveReasonController');
    
    //Routes for state/state service areas
    Route::resource('/states', 'StateController');
    
    //Routes for state/state service areas
    Route::resource('/cities', 'CityController');
    
    //Route for service state area
    Route::resource('/service-state-area', 'ServiceStateAreaController');
    
    //Routes for adjuster grade
    Route::resource('/adjuster-grades', 'AdjusterGradeController');
    
    //Routes for grade reasons
    Route::resource('/grade-reasons', 'GradeReasonController');
    
    //Routes for home states
    Route::resource('/home-state-license', 'AdjusterHomeStateLicenseController');
    Route::get('/home-state-license/single/edit/{state_license}', 'AdjusterHomeStateLicenseController@editSingle')->name('home-state-license-single.edit');
    
    //Route for role
    Route::resource('/roles', 'RoleController');
    
    //Route for search adjuster
    Route::get('/search/adjuster', 'SearchAdjusterController@index')->name('search.adjuster.index');
    Route::post('/search/adjuster', 'SearchAdjusterController@search')->name('search.adjuster');
    
    //Route for supporting forms main file
    Route::get('/supporting-forms', 'SupportingFormController@index')->name('supporting.form.index');
    
    //Routes for Languages
    Route::resource('/languages', 'LanguageController');
    
    //Routes for specialities
    Route::resource('/specialties', 'SpecialityController');
    
    //Route for carriers
    Route::resource('/carriers', 'CarrierController');
    
    //Route for Field Assign Source
    Route::resource('/field-assign-sources', "FieldAssignSourceController");
    
    //Route for Cl Assign Type
    Route::resource('/cl-assign-types', "ClAssignTypeController");

    //Route for personal status
    Route::resource('/personal-status', 'PersonalStatusController');
    
    //Route for Adjuster Correction
    Route::resource('/adjuster-corrections', "AdjusterCorrectionController");
    
    //Route for correction rejections
    Route::resource('/correction-rejections', 'CorrectionRejectionController');
    
    //Route for Log assignments
    Route::resource('/log-assignment', 'LogAssignmentController');

    Route::get('/log-fields', 'LogFieldAccessController@logfields')->name('logfields');
    Route::post('/log-fields/store', 'LogFieldAccessController@logstore')->name('logstore');
    
    //Routes for Claim Status
    Route::resource('/claim-status', 'ClaimStatusController');
    
    //Route for Tabs
    Route::get('/adjuster/{adjuster_id}/home-states', 'TabController@homeStates')->name('tab.adjuster.homestate');
    Route::get('/adjuster/{adjuster_id}/corrections', 'TabController@corrections')->name('tab.adjuster.corrections');
    Route::get('/adjuster/{adjuster_id}/rejections', 'TabController@rejections')->name('tab.adjuster.rejections');
    Route::get('/adjuster/{adjuster_id}/log-assignments', 'TabController@logAssignment')->name('tab.adjuster.log-assignments');
    Route::get('/adjuster/{adjuster_id}/claim-status', 'TabController@claimStatus')->name('tab.adjuster.claim-status');
    
    
    Route::get('/manage-dashboard', 'DashboardItemController@index')->name('dashboard.item.index');
    Route::post('/save-user-dashboard-item', 'DashboardItemController@store')->name('dashboard.item.user.store');

    //ajax routes
    Route::post('/get-cities', 'AjaxController@getCities')->name('get.cities');
    
});

