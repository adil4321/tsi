@extends('adminlte::page')
@section('title', 'Adjuster Correction')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">{{ $adj_correction->field_adjuster->getFullName() }} - {{ $adj_correction->field_adjuster->state->name }}</h3>
            <a href="{{ route('adjusters.edit', $adj_correction->field_adjuster->id) }}" style="float:right; color:white;">Edit</a>
        </div>
        <div class="col-md-12" style="padding: 10px;">
            {{-- <div class="col-md-1"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('adjusters.show', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">Adjuster Data</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.homestate', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">State Licenses</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.corrections', $adj_correction->field_adjuster->id) }}" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;" class="btn btn-primary btnTheme">Corrections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.rejections', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">Rejections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.log-assignments', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">Claim Log Assignments</a>
            <a href="{{ route('tab.adjuster.claim-status', $adj_correction->field_adjuster->id) }}" class="btn btn-primary btnTheme">Claim Status</a>

        </div>
        <div class="clearfix"></div>
        <div class="box-body table-responsive no-padding">
            <div class="box-header customHeader">
                <h3 class="box-title"><b>Claim Number: {{ $adj_correction->claim_number}}</b></h3>
                <a href="{{ route('adjuster-corrections.edit', $adj_correction->id) }}" style="float:right; color:white;">Edit</a>

            </div>
            <table class="table table-bordered tableStyle">
                <tbody>
                <tr>
                    <th>Review Date</th>
                    <td>{{ $adj_correction->review_date ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Catastrophe Claim</th>
                    <td>{{ ($adj_correction->catastrophe_claim ==1) ? "Yes" : 'No' }}</td>
                </tr>
                <tr>
                    <th>Carrier</th>
                    <td>{{ $adj_correction->carrier->name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Claim Number</th>
                    <td>{{ $adj_correction->claim_number ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Insd Last Name</th>
                    <td>{{ $adj_correction->insd_last_name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Field Adjuster</th>
                    <td>{{ $adj_correction->field_adjuster->getFullName() ?? "-" }}</td>
                </tr>
                <tr>
                    <th>QA</th>
                    <td>{{ $adj_correction->qa->name ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Leader</th>
                    <td>{{ $adj_correction->leader->name ?? "-" }}</td>
                </tr>
                <tr>
                    <th colspan="2"><h3>Field Adjuster Corrections</h3></th>
                </tr>
                <tr>
                    <th>FA GLR Corrections</th>
                    <td>{{ $adj_correction->fa_glr_correction ?? "-" }}</td>
                </tr>
                <tr>
                    <th>FA Est Corrections</th>
                    <td>{{ $adj_correction->fa_est_correction ?? "-" }}</td>
                </tr>
                <tr>
                    <th>FA Photo Corrections</th>
                    <td>{{ $adj_correction->fa_photo_correction ?? "-" }}</td>
                </tr>
                <tr>
                    <th>FA Total Corrections</th>
                    <td>{{ $adj_correction->fa_total_correction ?? "-" }}</td>
                </tr>
                <tr>
                    <th colspan="2"><h3>QA Corrections</h3></th>
                </tr>
                <tr>
                    <th>QA GLR Corrections</th>
                    <td>{{ $adj_correction->qa_glr_correction ?? "-" }}</td>
                </tr>
                <tr>
                    <th>QA Est Corrections</th>
                    <td>{{ $adj_correction->qa_est_correction ?? "-" }}</td>
                </tr>
                <tr>
                    <th>QA Photo Corrections</th>
                    <td>{{ $adj_correction->qa_photo_correction ?? "-" }}</td>
                </tr>
                <tr>
                    <th>QA Total Corrections</th>
                    <td>{{ $adj_correction->qa_total_correction ?? "-" }}</td>
                </tr>
                <tr>
                    <th>Correction Details</th>
                    <td>{{ $adj_correction->correction_detail ?? "-" }}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('css')
    <style type="text/css">
        .tableStyle th {
            width: 20%;
        }
    </style>
@endpush
