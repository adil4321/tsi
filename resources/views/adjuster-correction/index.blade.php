@extends('adminlte::page')
@section('title', 'All Adjuster Corrections')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Adjuster Corrections</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('adjuster-corrections.create') }}" class="btn btn-primary btnTheme">Create Adjuster Correction</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Review Date</th>
                    <th>Field Adjuster</th>
                    <th>Claim Number</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($adj_corrections))
                    @foreach($adj_corrections as $adj_correction)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $adj_correction->review_date ?? '--' }}</td>
                            <td>{{ $adj_correction->field_adjuster->getFullName() ?? '--' }}</td>
                            <td>{{ $adj_correction->claim_number ?? '--' }}</td>
                            <td>
                                <a href="{{ route('adjuster-corrections.edit', $adj_correction->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <a href="{{ route('adjuster-corrections.show', $adj_correction->id) }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                                <form action="{{ route('adjuster-corrections.destroy', $adj_correction->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': true,
                'pagelength': 10
            })

        });
    </script>
@endpush
