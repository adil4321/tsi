@extends('adminlte::page')
@section('title', 'Create User')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Create User</h3>
        </div>
        <form action="{{ route('users.store') }}" method="POST">
            @csrf
        <div class="box-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                        <label>First Name</label>
                        <input type="text" name="first_name" class="form-control">
                        @if($errors->has('first_name'))
                            <span class="help-block text-danger">{{ $errors->first('first_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                        <label>Last Name</label>
                        <input type="text" name="last_name" class="form-control">
                        @if($errors->has('last_name'))
                            <span class="help-block text-danger">{{ $errors->first('last_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
                        <label>User Name</label>
                        <input type="text" name="user_name" class="form-control">
                        @if($errors->has('user_name'))
                            <span class="help-block text-danger">{{ $errors->first('user_name') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control">
                        @if($errors->has('email'))
                            <span class="help-block text-danger">{{ $errors->first('email') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control">
                        @if($errors->has('password'))
                            <span class="help-block text-danger">{{ $errors->first('password') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <label>Confirm Password</label>
                        <input type="password" name="password_confirmation" class="form-control">
                        @if($errors->has('password_confirmation'))
                            <span class="help-block text-danger">{{ $errors->first('password_confirmation') }}</span>
                        @endif
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group {{ $errors->has('assign-roles') ? 'has-error' : '' }}">
                        <label>Role</label>
                        <select class="form-control" name="role">
                            @if(!empty($roles))
                                        @foreach($roles as $role)
                                                <option value="{{ $role->id }}" {{ ($role->id == old('role')) ? 'selected' : '' }}
                                                        selected="selected">{{ $role->name }}</option>
                                        @endforeach
                                    @endif
                        </select>
                        @if($errors->has('role'))
                            <span class="help-block text-danger">{{ $errors->first('role') }}</span>
                        @endif
                    </div>
                </div>
            </div>            
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btnTheme">Submit</button>
        </div>
        </form>
    </div>
@endsection
