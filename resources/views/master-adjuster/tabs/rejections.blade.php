@extends('adminlte::page')
@section('title', 'Data Form')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title"><b>{{ $adjuster->getFullName() ?? "-" }} - {{ $adjuster->state->name ?? "-" }}</b></h3>
            <a href="{{ route('adjusters.edit', $adjuster->id) }}" style="float:right; color:white;">Edit</a>
        </div>

        <div class="col-md-12" style="padding: 10px;">
            {{-- <div class="col-md-1"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('adjusters.show', $adjuster->id) }}"  class="btn btn-primary btnTheme">Adjuster Data</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.homestate', $adjuster->id) }}" class="btn btn-primary btnTheme">State Licenses</a>
            <a href="{{ route('tab.adjuster.corrections', $adjuster->id) }}" class="btn btn-primary btnTheme">Corrections</a>
            {{-- <div class="col-md-2"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.rejections', $adjuster->id) }}" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;" class="btn btn-primary btnTheme">Rejections</a>
            <a href="{{ route('tab.adjuster.log-assignments', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim Log Assignments</a>
            <a href="{{ route('tab.adjuster.claim-status', $adjuster->id) }}" class="btn btn-primary btnTheme">Claim Status</a>
            {{-- <div class="col-md-2"></div> --}}
        </div>
 <div class="clearfix"></div>
        <div class="box-body table-responsive no-padding">
            @if(!empty($adjuster->rejections))
                @foreach($adjuster->rejections as $correction_rejection)
                    <div class="box-header customHeader">
                        <h3 class="box-title"><b>Claim Number: {{ $correction_rejection->claim_number ?? "-" }}</b></h3>
                        <a href="{{ route('correction-rejections.edit', $correction_rejection->id) }}" style="float:right; color:white;">Edit</a>
                    </div>
                    <table class="table table-bordered tableStyle">
                        <tbody>
                        <tr>
                            <th>Review Date</th>
                            <td>{{ $correction_rejection->review_date ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Catastrophe Claim</th>
                            <td>
                                @if($correction_rejection->catastrophe_claim == 1)
                                    yes
                                @elseif($correction_rejection->catastrophe_claim == 0)
                                    no
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Carrier</th>
                            <td>{{ $correction_rejection->carrier->name ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Claim Number</th>
                            <td>{{ $correction_rejection->claim_number ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Insd Last Name</th>
                            <td>{{ $correction_rejection->insd_last_name ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Field Adjuster</th>
                            <td>{{ $adjuster->getFullName() ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>QA</th>
                            <td>{{ $correction_rejection->qa->name ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Leader</th>
                            <td>{{ $correction_rejection->leader->name ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Field GLR Number Exc</th>
                            <td>{{ $correction_rejection->field_glr_number_exc ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Field Est Number Exc</th>
                            <td>{{ $correction_rejection->field_est_number_exc ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Field Photo Number Exc</th>
                            <td>{{ $correction_rejection->field_photo_number_exc ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>Field Total Number Exc</th>
                            <td>{{ $correction_rejection->field_total_number_exc ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>QA GLR Number Exc</th>
                            <td>{{ $correction_rejection->qa_glr_number_exc ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>QA Est Number Exc</th>
                            <td>{{ $correction_rejection->qa_est_number_exc ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>QA Photo Number Exc</th>
                            <td>{{ $correction_rejection->qa_photo_number_exc ?? "-" }}</td>
                        </tr>
                        <tr>
                            <th>QA Total Number Exc</th>
                            <td>{{ $correction_rejection->qa_total_number_exc ?? "-" }}</td>
                        </tr>

                        <tr>
                            <th colspan="2"><h3>GLR Exceptions</h3></th>
                        </tr>
                        <tr>
                            <th>GLR Cov Narrative</th>
                            <td>
                                @if($correction_rejection->glr_cov_narrative == 1)
                                    Yes
                                @elseif($correction_rejection->glr_cov_narrative == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Date Ctc Inspected</th>
                            <td>
                                @if($correction_rejection->date_ctc_insp == 1)
                                    Yes
                                @elseif($correction_rejection->date_ctc_insp == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Mortgage Co</th>
                            <td>
                                @if($correction_rejection->mortgage_co == 1)
                                    Yes
                                @elseif($correction_rejection->mortgage_co == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>COL Cause of Loss</th>
                            <td>
                                @if($correction_rejection->col_cause_of_loss == 1)
                                    Yes
                                @elseif($correction_rejection->col_cause_of_loss == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Subro Salv Details</th>
                            <td>
                                @if($correction_rejection->subro_salv_details == 1)
                                    Yes
                                @elseif($correction_rejection->subro_salv_details == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Wind Hail Report</th>
                            <td>
                                @if($correction_rejection->wind_hail_rpt == 1)
                                    Yes
                                @elseif($correction_rejection->wind_hail_rpt == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>New Survey</th>
                            <td>
                                @if($correction_rejection->new_survey == 1)
                                    Yes
                                @elseif($correction_rejection->new_survey == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Logo Exception</th>
                            <td>
                                @if($correction_rejection->logo_exc == 1)
                                    Yes
                                @elseif($correction_rejection->logo_exc == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Other GLR Exception</th>
                            <td>
                                @if($correction_rejection->other_glr_exc == 1)
                                    Yes
                                @elseif($correction_rejection->other_glr_exc == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Current Xact</th>
                            <td>
                                @if($correction_rejection->current_xact == 1)
                                    Yes
                                @elseif($correction_rejection->current_xact == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2"><h3>Estimate Exceptions</h3></th>
                        </tr>
                        <tr>
                            <th>Deductible Application</th>
                            <td>
                                @if($correction_rejection->deductible_application == 1)
                                    Yes
                                @elseif($correction_rejection->deductible_application == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>No Cov/Dam Dele Part All</th>
                            <td>
                                @if($correction_rejection->no_cov_dam_dele_part_all == 1)
                                    Yes
                                @elseif($correction_rejection->no_cov_dam_dele_part_all == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Overhead and Profit</th>
                            <td>
                                @if($correction_rejection->overhead_and_profit == 1)
                                    Yes
                                @elseif($correction_rejection->overhead_and_profit == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Measure Accuracy</th>
                            <td>

                                @if($correction_rejection->measure_accuracy == 1)
                                    Yes
                                @elseif($correction_rejection->measure_accuracy == 0)
                                    No
                                @else
                                    -
                            @endif
                        </tr>
                        <tr>
                            <th>Sketch</th>
                            <td>
                                @if($correction_rejection->sketch == 1)
                                    Yes
                                @elseif($correction_rejection->sketch == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Repair vs Replace</th>
                            <td>
                                @if($correction_rejection->repair_vs_replace == 1)
                                    Yes
                                @elseif($correction_rejection->repair_vs_replace == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>ACV vs RCV</th>
                            <td>
                                @if($correction_rejection->acv_vs_rcv == 1)
                                    Yes
                                @elseif($correction_rejection->acv_vs_rcv == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Depreciation</th>
                            <td>
                                @if($correction_rejection->depreciation == 1)
                                    Yes
                                @elseif($correction_rejection->depreciation == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Line Item Exception</th>
                            <td>
                                @if($correction_rejection->line_item_exc == 1)
                                    Yes
                                @elseif($correction_rejection->line_item_exc == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Contents Exception</th>
                            <td>
                                @if($correction_rejection->contents_exc == 1)
                                    Yes
                                @elseif($correction_rejection->contents_exc == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Drywall</th>
                            <td>
                                @if($correction_rejection->drywall == 1)
                                    Yes
                                @elseif($correction_rejection->drywall == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Insulation</th>
                            <td>
                                @if($correction_rejection->insulation == 1)
                                    Yes
                                @elseif($correction_rejection->insulation == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Cleaning Mit</th>
                            <td>
                                @if($correction_rejection->cleaning_mit == 1)
                                    Yes
                                @elseif($correction_rejection->cleaning_mit == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Paint</th>
                            <td>
                                @if($correction_rejection->paint == 1)
                                    Yes
                                @elseif($correction_rejection->paint == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Trim and Base</th>
                            <td>
                                @if($correction_rejection->trim_and_base == 1)
                                    Yes
                                @elseif($correction_rejection->trim_and_base == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Flooring</th>
                            <td>
                                @if($correction_rejection->flooring == 1)
                                    Yes
                                @elseif($correction_rejection->flooring == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Cabinets</th>
                            <td>
                                @if($correction_rejection->cabinets == 1)
                                    Yes
                                @elseif($correction_rejection->cabinets == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Debris Removal</th>
                            <td>
                                @if($correction_rejection->debris_removal == 1)
                                    Yes
                                @elseif($correction_rejection->debris_removal == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Other Estimated Exception</th>
                            <td>
                                @if($correction_rejection->other_est_exc == 1)
                                    Yes
                                @elseif($correction_rejection->other_est_exc == 0)
                                    NO
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Oth Structure Proper Items</th>
                            <td>
                                @if($correction_rejection->oth_structure_proper_items == 1)
                                    Yes
                                @elseif($correction_rejection->oth_structure_proper_items == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Oth Str ACV vs RCV</th>
                            <td>
                                @if($correction_rejection->oth_str_acv_vs_rcv == 1)
                                    Yes
                                @elseif($correction_rejection->oth_str_acv_vs_rcv == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Oth Str Other Exception</th>
                            <td>
                                @if($correction_rejection->oth_str_other_exc == 1)
                                    Yes
                                @elseif($correction_rejection->oth_str_other_exc == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Roof Wind BP</th>
                            <td>
                                @if($correction_rejection->roof_wind_bp == 1)
                                    Yes
                                @elseif($correction_rejection->roof_wind_bp == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Roof Hail BP</th>
                            <td>
                                @if($correction_rejection->roof_hail_bp == 1)
                                    Yes
                                @elseif($correction_rejection->roof_hail_bp == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Roof Valley or Ridge</th>
                            <td>
                                @if($correction_rejection->roof_valley_or_ridge == 1)
                                    Yes
                                @elseif($correction_rejection->roof_valley_or_ridge == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Roof Repair vs Replace</th>
                            <td>
                                @if($correction_rejection->roof_repair_vs_replace == 1)
                                    Yes
                                @elseif($correction_rejection->roof_repair_vs_replace == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Roof Meas or Quantity</th>
                            <td>
                                @if($correction_rejection->roof_meas_or_quantity == 1)
                                    Yes
                                @elseif($correction_rejection->roof_meas_or_quantity == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Roof Miss Items</th>
                            <td>
                                @if($correction_rejection->roof_miss_items == 1)
                                    Yes
                                @elseif($correction_rejection->roof_miss_items == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Roof Add Labor</th>
                            <td>
                                @if($correction_rejection->roof_add_labor == 1)
                                    Yes
                                @elseif($correction_rejection->roof_add_labor == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Roof Other Exception</th>
                            <td>
                                @if($correction_rejection->roof_other_exc == 1)
                                    Yes
                                @elseif($correction_rejection->roof_other_exc == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2"><h3>Photo Exceptions</h3></th>
                        </tr>
                        <tr>
                            <th>Photo BP</th>
                            <td>
                                @if($correction_rejection->photo_bp == 1)
                                    Yes
                                @elseif($correction_rejection->photo_bp == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Photo Clear</th>
                            <td>
                                @if($correction_rejection->photo_clear == 1)
                                    Yes
                                @elseif($correction_rejection->photo_clear == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Photo All Damage</th>
                            <td>
                                @if($correction_rejection->photo_all_damage == 1)
                                    Yes
                                @elseif($correction_rejection->photo_all_damage == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Photo Label</th>
                            <td>
                                @if($correction_rejection->photo_label == 1)
                                    Yes
                                @elseif($correction_rejection->photo_label == 0)
                                    No
                                @else
                                    -
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Exception Detail</th>
                            <td>{{ $correction_rejection->exception_details ?? "-" }}</td>
                        </tr>
                        </tbody>
                    </table>
                @endforeach
            @endif
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('css')
    <style type="text/css">
        .tableStyle th
        {
            width: 20%;
        }
    </style>
@endpush

