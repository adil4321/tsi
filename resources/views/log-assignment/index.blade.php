@extends('adminlte::page')
@section('title', 'All Log Assignments')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Log Assignments</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('log-assignment.create') }}" class="btn btn-primary btnTheme">Create Log Assignments</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    @if($form_fields->claim_number==true)<th>Claim Number</th>@endif
                    @if($form_fields->master_adjuster_id==true)<th>Adjuster</th>@endif
                    @if($form_fields->tsi_file_number==true)<th>File Number</th>@endif
                    @if($form_fields->city_id==true)<th>City</th>@endif
                    @if($form_fields->state_id==true)<th>State</th>@endif
                    @if($form_fields->zip_code==true)<th>Zip Code</th>@endif
                    @if($form_fields->date==true)<th>Date</th>@endif
                    @if($form_fields->company==true)<th>Company</th>@endif
                    @if($form_fields->status==true)<th>Status</th>@endif
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($log_assignments))
                    @foreach($log_assignments as $log_assignment)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            @if($form_fields->claim_number==true)<td>{{ $log_assignment->claim_number ?? '--' }}</td>@endif
                            @if($form_fields->master_adjuster_id==true)<td>{{ $log_assignment->master_adjuster->getFullName() ?? '--' }}</td> @endif
                            @if($form_fields->tsi_file_number==true)<td>{{ $log_assignment->tsi_file_number ?? '--' }}</td>@endif
                            @if($form_fields->city_id==true)<td>{{ $log_assignment->city_id ?? '--' }}</td>@endif
                            @if($form_fields->state_id==true)<td>{{ $log_assignment->state_id ?? '--' }}</td>@endif
                            @if($form_fields->zip_code==true)<td>{{ $log_assignment->zip_code ?? '--' }}</td>@endif
                            @if($form_fields->date==true)<td>{{ $log_assignment->date ?? '--' }}</td>@endif
                            @if($form_fields->company==true)<td>{{ $log_assignment->company ?? '--' }}</td>@endif
                            @if($form_fields->status==true)<td>{{ ($log_assignment->status == 0) ? 'close' : 'open'}}</td>@endif
                            <td>
                                <a href="{{ route('log-assignment.edit', $log_assignment->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <a href="{{ route('log-assignment.show', $log_assignment->id) }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                                <form action="{{ route('log-assignment.destroy', $log_assignment->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': true,
                'pagelength': 10
            })

        });
    </script>
@endpush
