@extends('adminlte::page')
@section('title', 'All Claim Status')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Claim Status</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('claim-status.create') }}" class="btn btn-primary btnTheme">Create Claim Status</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>TSI Number</th>
                    <th>Adjuster</th>
                    <th>Claim Number</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($claim_statuses))
                    @foreach($claim_statuses as $claim_status)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $claim_status->tsi_number ?? '--' }}</td>
                            <td>{{ $claim_status->master_adjuster->getFullName() ?? '--' }}</td>
                            <td>{{ $claim_status->claim_number }}</td>
                            <td>
                                <a href="{{ route('claim-status.edit', $claim_status->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <a href="{{ route('claim-status.show', $claim_status->id) }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
                                <form action="{{ route('claim-status.destroy', $claim_status->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': true,
                'pagelength': 10
            })

        });
    </script>
@endpush
