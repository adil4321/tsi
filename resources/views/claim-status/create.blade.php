@extends('adminlte::page')
@section('title', 'Claim Status')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">Claim Status Form</h3>
        </div>
        <form action="{{ route('claim-status.store') }}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('tsi_number') ? 'has-error': '' }}">
                                <label>TSI Number</label>
                                <input type="text" name="tsi_number" class="form-control" value="{{ old('tsi_number') }}">
                                @if($errors->has('tsi_number'))
                                    <span class="help-block text-danger">{{ $errors->first('tsi_number') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('date_received') ? 'has-error': '' }}">
                            <label>Date Received</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                <input type="text" class="form-control datepicker" name="date_received" value="{{ old('date_received') }}">
                                @if($errors->has('date_received'))
                                    <span class="help-block text-danger">{{ $errors->first('date_received') }}</span>
                                @endif
                            </div>
                        </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('adjuster') ? 'has-error': '' }}">
                                <label>Adjuster</label>
                                <select name="adjuster" class="form-control">
                                    <option value="">--Select--</option>
                                    @if(!empty($adjusters))
                                        @foreach($adjusters as $adjuster)
                                            <option value="{{ $adjuster->id }}" {{ (old('adjuster') == $adjuster->id) ? 'selected' : '' }}>{{ $adjuster->getFullName() }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('adjuster'))
                                    <span class="help-block text-danger">{{ $errors->first('adjuster') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('insured') ? 'has-error': '' }}">
                                <label>Insured</label>
                                <input type="text" name="insured" class="form-control" value="{{ old('insured') }}">
                                @if($errors->has('insured'))
                                    <span class="help-block text-danger">{{ $errors->first('insured') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('company') ? 'has-error': '' }}">
                                <label>Company</label>
                                <input type="text" name="company" class="form-control" value="{{ old('company') }}">
                                @if($errors->has('insured'))
                                    <span class="help-block text-danger">{{ $errors->first('company') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('claim_number') ? 'has-error': '' }}">
                                <label>Claim Number</label>
                                <input type="text" name="claim_number" class="form-control" value="{{ old('claim_number') }}">
                                @if($errors->has('claim_number'))
                                    <span class="help-block text-danger">{{ $errors->first('claim_number') }}</span>
                                @endif
                            </div>
                        </div>
                    
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('date_of_notice') ? 'has-error': '' }}">
                                <label>Date of notice</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control datepicker" name="date_of_notice" value="{{ old('date_of_notice') }}">
                                    @if($errors->has('date_of_notice'))
                                        <span class="help-block text-danger">{{ $errors->first('date_of_notice') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('date_contacted') ? 'has-error': '' }}">
                                <label>Date Contacted</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control datepicker" name="date_contacted" value="{{ old('date_contacted') }}">
                                    @if($errors->has('date_contacted'))
                                        <span class="help-block text-danger">{{ $errors->first('date_contacted') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('date_inspected') ? 'has-error': '' }}">
                                <label>Date Inspected</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" class="form-control datepicker" name="date_inspected" value="{{ old('date_inspected') }}">
                                    @if($errors->has('date_inspected'))
                                        <span class="help-block text-danger">{{ $errors->first('date_inspected') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('report_due') ? 'has-error': '' }}">
                                <label>Report Due</label>
                                <input type="text" name="report_due" class="form-control" value="{{ old('report_due') }}">
                                @if($errors->has('report_due'))
                                    <span class="help-block text-danger">{{ $errors->first('report_due') }}</span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="note">Note</label><br>
                                <textarea name="note" id="note" class="form-control">{{ old('note') }}</textarea>
                            </div>
                        </div>
                    </div>      
                </div>
            </div> 
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Save</button>
            </div>
        </form>
    </div>
@endsection
