@extends('adminlte::page')
@section('title', 'Edit Correction Rejections')

@section('content')
    <div class="box">
        <div class="box-header customHeader">
            <h3 class="box-title">{{ $correction_rejection->field_adjuster->getFullName() }} - {{ $correction_rejection->field_adjuster->state->name }}</h3>
            <a href="{{ route('adjusters.show', $correction_rejection->field_adjuster->id) }}" style="float:right; color:white;">View</a>
        </div>

        <div class="col-md-12" style="padding: 10px;">
            {{-- <div class="col-md-1"></div> --}}
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('adjusters.show', $correction_rejection->field_adjuster->id) }}" class="btn btn-primary btnTheme">Adjuster Data</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.homestate', $correction_rejection->field_adjuster->id) }}" class="btn btn-primary btnTheme">State Licenses</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.corrections', $correction_rejection->field_adjuster->id) }}" class="btn btn-primary btnTheme">Corrections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.rejections', $correction_rejection->field_adjuster->id) }}" style="background-color: #dd4b39 !important;border-color: #dd4b39 !important;" class="btn btn-primary btnTheme">Rejections</a>
            {{-- <div class="col-md-2"></div> --}}
            <a href="{{ route('tab.adjuster.log-assignments', $correction_rejection->field_adjuster->id) }}" class="btn btn-primary btnTheme">Claim Log Assignments</a>
            <a href="{{ route('tab.adjuster.claim-status', $correction_rejection->field_adjuster->id) }}" class="btn btn-primary btnTheme">Claim Status</a>

        </div>
        <div class="clearfix"></div>
        <form action="{{ route('correction-rejections.update', $correction_rejection->id) }}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="box-header customHeader">
                <h3 class="box-title"><b>Claim Number: {{ $correction_rejection->claim_number}}</b></h3>
                    <a href="{{ route('correction-rejections.show', $correction_rejection->id) }}" style="float:right; color:white;">View</a>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('review_date') ? 'has-error': '' }}">
                                <label>Review Date</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="review_date" class="form-control datepicker" value="{{ $correction_rejection->review_date }}" required="">
                                    @if($errors->has('review_date'))
                                        <span class="help-block text-danger">{{ $errors->first('review_date') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('catastrophe_claim') ? 'has-error': '' }}">
                                <label>Catastrophe Claim</label>
                                <select name="catastrophe_claim" class="form-control" required="required">
                                    <option value="" {{ ($correction_rejection->catastrophe_claim == null) ? 'selected' : '' }}>--Select--</option>
                                    <option value="yes" {{ ($correction_rejection->catastrophe_claim == 1) ? 'selected' : '' }}>Yes</option>
                                    <option value="no" {{ ($correction_rejection->catastrophe_claim == 0) ? 'selected' : '' }}>No</option>
                                </select>
                                @if($errors->has('catastrophe_claim'))
                                    <span
                                        class="help-block text-danger">{{ $errors->first('catastrophe_claim') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('carrier') ? 'has-error': '' }}">
                                <label>Carrier</label>
                                <select name="carrier" class="form-control">
                                    <option value="">-Select--</option>
                                    @if(!empty($carriers))
                                        @foreach($carriers as $carrier)
                                            <option value="{{ $carrier->id }}" {{ ($correction_rejection->carrier_id == $carrier->id) ? 'selected' : '' }}>{{ $carrier->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('carrier'))
                                    <span class="help-block text-danger">{{ $errors->first('carrier') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('claim_number') ? 'has-error': '' }}">
                                <label>Claim Number</label>
                                <input type="text" name="claim_number" class="form-control" value="{{ $correction_rejection->claim_number }}" required="">
                                @if($errors->has('claim_number'))
                                    <span class="help-block text-danger">{{ $errors->first('claim_number') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('insd_last_name') ? 'has-error': '' }}">
                                <label>Insd Last Name</label>
                                <input type="text" name="insd_last_name" class="form-control" value="{{ $correction_rejection->insd_last_name }}">
                                @if($errors->has('insd_last_name'))
                                    <span class="help-block text-danger">{{ $errors->first('insd_last_name') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('field_adjuster') ? 'has-error': '' }}">
                                <label>Field Adjuster</label>
                                <select name="adjuster_id" class="form-control" required="required">
                                    <option value="">--Select--</option>
                                    @if(!empty($adjusters))
                                        @foreach($adjusters as $adjuster)
                                            <option value="{{ $adjuster->id }}" {{ ($correction_rejection->adjuster_id == $adjuster->id) ? 'selected' : '' }}>{{ $adjuster->getFullName() }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('field_adjuster'))
                                    <span class="help-block text-danger">{{ $errors->first('field_adjuster') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('qa') ? 'has-error': '' }}">
                                <label>QA</label>
                                <select name="qa" class="form-control">
                                    <option value="">--Select--</option>
                                    @if(!empty($qas))
                                        @foreach($qas as $qa)
                                            <option value="{{ $qa->id }}" {{ ($correction_rejection->qa_id == $qa->id) ? 'selected' : '' }}>{{ $qa->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('qa'))
                                    <span class="help-block text-danger">{{ $errors->first('qa') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('leader') ? 'has-error': '' }}">
                                <label>Leader</label>
                                <select name="leader" class="form-control">
                                    <option value="">--Select--</option>
                                    @if(!empty($leaders))
                                        @foreach($leaders as $leader)
                                            <option value="{{ $leader->id }}" {{ ($correction_rejection->leader_id == $leader->id) ? 'selected' : '' }}>{{ $leader->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                @if($errors->has('leader'))
                                    <span class="help-block text-danger">{{ $errors->first('leader') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('fld_glr_number_exc') ? 'has-error': '' }}">
                                        <label>Field GLR Number Exc</label>
                                        <input type="text" name="fld_glr_number_exc" class="form-control" value="{{ $correction_rejection->field_glr_number_exc }}">
                                        @if($errors->has('fld_glr_number_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('fld_glr_number_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('fld_est_number_exc') ? 'has-error': '' }}">
                                        <label>Field Est Number Exc</label>
                                        <input type="text" name="fld_est_number_exc" class="form-control" value="{{ $correction_rejection->field_est_number_exc }}">
                                        @if($errors->has('fld_est_number_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('fld_est_number_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('fld_photo_number_exc') ? 'has-error': '' }}">
                                        <label>Field Photo Number Exc</label>
                                        <input type="text" name="fld_photo_number_exc" class="form-control" value="{{ $correction_rejection->field_photo_number_exc }}">
                                        @if($errors->has('fld_photo_number_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('fld_photo_number_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('fld_total_number_exc') ? 'has-error': '' }}">
                                        <label>Field Total Number Exc</label>
                                        <input type="text" name="fld_total_number_exc" class="form-control" value="{{ $correction_rejection->field_total_number_exc }}">
                                        @if($errors->has('fld_total_number_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('fld_total_number_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('qa_glr_number_exc') ? 'has-error': '' }}">
                                        <label>QA GLR Number Exc</label>
                                        <input type="text" name="qa_glr_number_exc" class="form-control" value="{{ $correction_rejection->qa_glr_number_exc }}">
                                        @if($errors->has('qa_glr_number_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('qa_glr_number_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('qa_est_number_exc') ? 'has-error': '' }}">
                                        <label>QA Est Number Exc</label>
                                        <input type="text" name="qa_est_number_exc" class="form-control" value="{{ $correction_rejection->qa_est_number_exc }}">
                                        @if($errors->has('qa_est_number_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('qa_est_number_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('qa_photo_number_exc') ? 'has-error': '' }}">
                                        <label>QA Photo Number Exc</label>
                                        <input type="text" name="qa_photo_number_exc" class="form-control" value="{{ $correction_rejection->qa_photo_number_exc }}">
                                        @if($errors->has('qa_photo_number_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('qa_photo_number_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group {{ $errors->has('qa_total_number_exc') ? 'has-error': '' }}">
                                        <label>QA Total Number Exc</label>
                                        <input type="text" name="qa_total_number_exc" class="form-control" value="{{ $correction_rejection->qa_total_number_exc }}">
                                        @if($errors->has('qa_total_number_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('qa_total_number_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4><b>GLR Exceptions</b></h4>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('glr_cov_narrative') ? 'has-error': '' }}">
                                        <label>GLR Cov Narrative</label>
                                        <select class="form-control" name="glr_cov_narrative">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->glr_cov_narrative == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->glr_cov_narrative == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('glr_cov_narrative'))
                                            <span class="help-block text-danger">{{ $errors->first('glr_cov_narrative') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('date_ctc_insp') ? 'has-error': '' }}">
                                        <label>Date Ctc Inspected</label>
                                        <select class="form-control" name="date_ctc_insp">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->date_ctc_insp == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->date_ctc_insp == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('date_ctc_insp'))
                                            <span class="help-block text-danger">{{ $errors->first('date_ctc_insp') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('mortgage_co') ? 'has-error': '' }}">
                                        <label>Mortgage Co</label>
                                        <select class="form-control" name="mortgage_co">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->mortgage_co == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->mortgage_co == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('mortgage_co'))
                                            <span class="help-block text-danger">{{ $errors->first('mortgage_co') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('col_cause_of_loss') ? 'has-error': '' }}">
                                        <label>COL Cause of Loss</label>
                                        <select class="form-control" name="col_cause_of_loss">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->col_cause_of_loss == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->col_cause_of_loss == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('col_cause_of_loss'))
                                            <span class="help-block text-danger">{{ $errors->first('col_cause_of_loss') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('subro_salv_details') ? 'has-error': '' }}">
                                        <label>Subro Salv Details</label>
                                        <select class="form-control" name="subro_salv_details">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->subro_salv_details == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->subro_salv_details == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('subro_salv_details'))
                                            <span class="help-block text-danger">{{ $errors->first('subro_salv_details') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('wind_hail_rpt') ? 'has-error': '' }}">
                                        <label>Wind Hail Report</label>
                                        <select class="form-control" name="wind_hail_rpt">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->wind_hail_rpt == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->wind_hail_rpt == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('wind_hail_rpt'))
                                            <span class="help-block text-danger">{{ $errors->first('wind_hail_rpt') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('new_survey') ? 'has-error': '' }}">
                                        <label>New Survey</label>
                                        <select class="form-control" name="new_survey">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->new_survey == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->new_survey == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('new_survey'))
                                            <span class="help-block text-danger">{{ $errors->first('new_survey') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('logo_exc') ? 'has-error': '' }}">
                                        <label>Logo Exception</label>
                                        <select class="form-control" name="logo_exc">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->logo_exc == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->logo_exc == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('logo_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('logo_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('other_glr_exc') ? 'has-error': '' }}">
                                        <label>Other GLR Exc.</label>
                                        <select class="form-control" name="other_glr_exc">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->other_glr_exc == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->other_glr_exc == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('other_glr_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('other_glr_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('current_xact') ? 'has-error': '' }}">
                                        <label>Current Xact</label>
                                        <select class="form-control" name="current_xact">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->current_xact == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->current_xact == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('current_xact'))
                                            <span class="help-block text-danger">{{ $errors->first('current_xact') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4><b>Estimate Exceptions</b></h4>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('deductible_application') ? 'has-error': '' }}">
                                        <label>Deductible Application</label>
                                        <select class="form-control" name="deductible_application">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->deductible_application == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->deductible_application == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('deductible_application'))
                                            <span class="help-block text-danger">{{ $errors->first('deductible_application') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('no_cov_dam_dele_part_all') ? 'has-error': '' }}">
                                        <label>No Cov/Dam Dele Part All</label>
                                        <select class="form-control" name="no_cov_dam_dele_part_all">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->no_cov_dam_dele_part_all == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->no_cov_dam_dele_part_all == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('no_cov_dam_dele_part_all'))
                                            <span class="help-block text-danger">{{ $errors->first('no_cov_dam_dele_part_all') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('overhead_and_profit') ? 'has-error': '' }}">
                                        <label>Overhead and Profit</label>
                                        <select class="form-control" name="overhead_and_profit">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->overhead_and_profit == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->overhead_and_profit == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('overhead_and_profit'))
                                            <span class="help-block text-danger">{{ $errors->first('overhead_and_profit') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('measure_accuracy') ? 'has-error': '' }}">
                                        <label>Measure Accuracy</label>
                                        <select class="form-control" name="measure_accuracy">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->measure_accuracy == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->measure_accuracy == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('measure_accuracy'))
                                            <span class="help-block text-danger">{{ $errors->first('measure_accuracy') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('sketch') ? 'has-error': '' }}">
                                        <label>Sketch</label>
                                        <select class="form-control" name="sketch">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->sketch == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->sketch == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('sketch'))
                                            <span class="help-block text-danger">{{ $errors->first('sketch') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('repair_vs_replace') ? 'has-error': '' }}">
                                        <label>Repair vs Replace</label>
                                        <select class="form-control" name="repair_vs_replace">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->repair_vs_replace == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->repair_vs_replace == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('repair_vs_replace'))
                                            <span class="help-block text-danger">{{ $errors->first('repair_vs_replace') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('acv_vs_rcv') ? 'has-error': '' }}">
                                        <label>ACV vs RCV</label>
                                        <select class="form-control" name="acv_vs_rcv">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->acv_vs_rcv == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->acv_vs_rcv == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('acv_vs_rcv'))
                                            <span class="help-block text-danger">{{ $errors->first('acv_vs_rcv') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('depreciation') ? 'has-error': '' }}">
                                        <label>Depreciation</label>
                                        <select class="form-control" name="depreciation">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->depreciation == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->depreciation == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('depreciation'))
                                            <span class="help-block text-danger">{{ $errors->first('depreciation') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('line_item_exc') ? 'has-error': '' }}">
                                        <label>Line Item Exc</label>
                                        <select class="form-control" name="line_item_exc">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->line_item_exc == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->line_item_exc == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('line_item_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('line_item_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('contents_exc') ? 'has-error': '' }}">
                                        <label>Contents Exc</label>
                                        <select class="form-control" name="contents_exc">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->contents_exc == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->contents_exc == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('contents_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('contents_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('drywall') ? 'has-error': '' }}">
                                        <label>Drywall</label>
                                        <select class="form-control" name="drywall">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->drywall == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->drywall == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('drywall'))
                                            <span class="help-block text-danger">{{ $errors->first('drywall') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('insulation') ? 'has-error': '' }}">
                                        <label>Insulation</label>
                                        <select class="form-control" name="insulation">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->insulation == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->insulation == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('insulation'))
                                            <span class="help-block text-danger">{{ $errors->first('insulation') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('cleaning_mit') ? 'has-error': '' }}">
                                        <label>Cleaning Mit</label>
                                        <select class="form-control" name="cleaning_mit">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->cleaning_mit == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->cleaning_mit == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('cleaning_mit'))
                                            <span class="help-block text-danger">{{ $errors->first('cleaning_mit') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('paint') ? 'has-error': '' }}">
                                        <label>Paint</label>
                                        <select class="form-control" name="paint">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->paint == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->paint == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('paint'))
                                            <span class="help-block text-danger">{{ $errors->first('paint') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('trim_and_base') ? 'has-error': '' }}">
                                        <label>Trim and Base</label>
                                        <select class="form-control" name="trim_and_base">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->trim_and_base == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->trim_and_base == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('trim_and_base'))
                                            <span class="help-block text-danger">{{ $errors->first('trim_and_base') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('flooring') ? 'has-error': '' }}">
                                        <label>Flooring</label>
                                        <select class="form-control" name="flooring">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->flooring == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->flooring == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('flooring'))
                                            <span class="help-block text-danger">{{ $errors->first('flooring') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('cabinets') ? 'has-error': '' }}">
                                        <label>Cabinets</label>
                                        <select class="form-control" name="cabinets">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->cabinets == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->cabinets == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('cabinets'))
                                            <span class="help-block text-danger">{{ $errors->first('cabinets') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('debris_removal') ? 'has-error': '' }}">
                                        <label>Debris Removal</label>
                                        <select class="form-control" name="debris_removal">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->debris_removal == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->debris_removal == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('debris_removal'))
                                            <span class="help-block text-danger">{{ $errors->first('debris_removal') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('other_est_exc') ? 'has-error': '' }}">
                                        <label>Other Est Exc</label>
                                        <select class="form-control" name="other_est_exc">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->other_est_exc == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->other_est_exc == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('other_est_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('other_est_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('oth_structure_proper_items') ? 'has-error': '' }}">
                                        <label>Oth Structure Proper Items</label>
                                        <select class="form-control" name="oth_structure_proper_items">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->oth_structure_proper_items == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->oth_structure_proper_items == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('oth_structure_proper_items'))
                                            <span class="help-block text-danger">{{ $errors->first('oth_structure_proper_items') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('oth_str_acv_vs_rcv') ? 'has-error': '' }}">
                                        <label>Oth Str ACV vs RCV</label>
                                        <select class="form-control" name="oth_str_acv_vs_rcv">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->oth_str_acv_vs_rcv == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->oth_str_acv_vs_rcv == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('oth_str_acv_vs_rcv'))
                                            <span class="help-block text-danger">{{ $errors->first('oth_str_acv_vs_rcv') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('oth_str_other_exc') ? 'has-error': '' }}">
                                        <label>Oth Str Other Exc</label>
                                        <select class="form-control" name="oth_str_other_exc">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->oth_str_other_exc == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->oth_str_other_exc == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('oth_str_other_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('oth_str_other_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('roof_wind_bp') ? 'has-error': '' }}">
                                        <label>Roof Wind BP</label>
                                        <select class="form-control" name="roof_wind_bp">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->roof_wind_bp == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->roof_wind_bp == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('roof_wind_bp'))
                                            <span class="help-block text-danger">{{ $errors->first('roof_wind_bp') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('roof_hail_bp') ? 'has-error': '' }}">
                                        <label>Roof Hail BP</label>
                                        <select class="form-control" name="roof_hail_bp">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->roof_hail_bp == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->roof_hail_bp == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('roof_hail_bp'))
                                            <span class="help-block text-danger">{{ $errors->first('roof_hail_bp') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('roof_valley_or_ridge') ? 'has-error': '' }}">
                                        <label>Roof Valley or Ridge</label>
                                        <select class="form-control" name="roof_valley_or_ridge">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->roof_valley_or_ridge == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->roof_valley_or_ridge == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('roof_valley_or_ridge'))
                                            <span class="help-block text-danger">{{ $errors->first('roof_valley_or_ridge') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('roof_repair_vs_replace') ? 'has-error': '' }}">
                                        <label>Roof Repair vs Replace</label>
                                        <select class="form-control" name="roof_repair_vs_replace">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->roof_repair_vs_replace == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->roof_repair_vs_replace == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('roof_repair_vs_replace'))
                                            <span class="help-block text-danger">{{ $errors->first('roof_repair_vs_replace') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('roof_meas_or_quantity') ? 'has-error': '' }}">
                                        <label>Roof Meas or Quantity</label>
                                        <select class="form-control" name="roof_meas_or_quantity">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->roof_meas_or_quantity == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->roof_meas_or_quantity == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('roof_meas_or_quantity'))
                                            <span class="help-block text-danger">{{ $errors->first('roof_meas_or_quantity') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('roof_miss_items') ? 'has-error': '' }}">
                                        <label>Roof Miss Items</label>
                                        <select class="form-control" name="roof_miss_items">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->roof_miss_items == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->roof_miss_items == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('roof_miss_items'))
                                            <span class="help-block text-danger">{{ $errors->first('roof_miss_items') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('roof_add_labor') ? 'has-error': '' }}">
                                        <label>Roof Add Labor</label>
                                        <select class="form-control" name="roof_add_labor">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->roof_add_labor == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->roof_add_labor == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('roof_add_labor'))
                                            <span class="help-block text-danger">{{ $errors->first('roof_add_labor') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('roof_other_exc') ? 'has-error': '' }}">
                                        <label>Roof Other Exc</label>
                                        <select class="form-control" name="roof_other_exc">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->roof_other_exc == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->roof_other_exc == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('roof_other_exc'))
                                            <span class="help-block text-danger">{{ $errors->first('roof_other_exc') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h4><b>Photo Exceptions</b></h4>
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('photo_bp') ? 'has-error': '' }}">
                                        <label>Photo BP</label>
                                        <select class="form-control" name="photo_bp">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->photo_bp == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->photo_bp == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('photo_bp'))
                                            <span class="help-block text-danger">{{ $errors->first('photo_bp') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('photo_clear') ? 'has-error': '' }}">
                                        <label>Photo Clear</label>
                                        <select class="form-control" name="photo_clear">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->photo_clear == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->photo_clear == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('photo_clear'))
                                            <span class="help-block text-danger">{{ $errors->first('photo_clear') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('photo_all_damage') ? 'has-error': '' }}">
                                        <label>Photo All Damage</label>
                                        <select class="form-control" name="photo_all_damage">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->photo_all_damage == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->photo_all_damage == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('photo_all_damage'))
                                            <span class="help-block text-danger">{{ $errors->first('photo_all_damage') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group {{ $errors->has('photo_label') ? 'has-error': '' }}">
                                        <label>Photo Label</label>
                                        <select class="form-control" name="photo_label">
                                            <option value="">--Select--</option>
                                            <option value="1" {{ ($correction_rejection->photo_label == '1') ? 'selected' : '' }}>Yes</option>
                                            <option value="0" {{ ($correction_rejection->photo_label == '0') ? 'selected' : '' }}>No</option>
                                        </select>
                                        @if($errors->has('photo_label'))
                                            <span class="help-block text-danger">{{ $errors->first('photo_label') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group {{ $errors->has('exception_details') ? 'has-error': '' }}">
                                <label>Exception Detail</label>
                                <textarea class="form-control" rows="5" name="exception_details">{{ $correction_rejection->exception_details }}</textarea>
                                @if($errors->has('exception_details'))
                                    <span class="help-block text-danger">{{ $errors->first('exception_details') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary btnTheme">Submit</button>
                </div>
        </form>
    </div>
@endsection
