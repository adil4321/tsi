@extends('adminlte::page')
@section('title', 'Add Inactive Reason')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Add New Inactive Reason</h3>
        </div>
        <form action="{{ route('inactive-reasons.store') }}" method="POST">
            @csrf
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('reason') ? 'has-error' : '' }}">
                            <label>Reason</label>
                            <input type="text" name="reason" class="form-control">
                            @if($errors->has('reason'))
                                <span class="help-block text-danger">{{ $errors->first('reason') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Submit</button>
            </div>
        </form>
    </div>
@endsection
