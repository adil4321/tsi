@extends('adminlte::page')
@section('title', 'Edit Inactive Reason')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit Inactive Reason</h3>
        </div>
        <form action="{{ route('inactive-reasons.update', $reason->id) }}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('reason') ? 'has-error' : '' }}">
                            <label>Reason</label>
                            <input type="text" name="reason" value="{{ $reason->name }}" class="form-control">
                            @if($errors->has('reason'))
                                <span class="help-block text-danger">{{ $errors->first('reason') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Submit</button>
            </div>
        </form>
    </div>
@endsection
