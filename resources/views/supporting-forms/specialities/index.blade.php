@extends('adminlte::page')
@section('title', 'All Specialities')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Specialities</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('specialties.create') }}" class="btn btn-primary btnTheme">Create Speciality</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($specialities))
                    @foreach($specialities as $speciality)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $speciality->name ?? '--' }}</td>
                            <td>
                                <a href="{{ route('specialties.edit', $speciality->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <form action="{{ route('specialties.destroy', $speciality->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': false,
            })

        });
    </script>
@endpush
