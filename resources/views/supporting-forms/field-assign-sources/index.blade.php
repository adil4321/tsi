@extends('adminlte::page')
@section('title', 'All Field Assign Sources')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Field Assign Sources</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('field-assign-sources.create') }}" class="btn btn-primary btnTheme">Create Field Assign Source</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($field_assign_sources))
                    @foreach($field_assign_sources as $field_assign_source)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $field_assign_source->name ?? '--' }}</td>
                            <td>
                                <a href="{{ route('field-assign-sources.edit', $field_assign_source->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <form action="{{ route('field-assign-sources.destroy', $field_assign_source->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': false,
            })

        });
    </script>
@endpush
