@extends('adminlte::page')
@section('title', 'All Carriers')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Carriers</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('carriers.create') }}" class="btn btn-primary btnTheme">Create Carrier</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Carrier</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($carriers))
                    @foreach($carriers as $carrier)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $carrier->name ?? '--' }}</td>
                            <td>
                                <a href="{{ route('carriers.edit', $carrier->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <form action="{{ route('carriers.destroy', $carrier->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': false,
            })

        });
    </script>
@endpush
