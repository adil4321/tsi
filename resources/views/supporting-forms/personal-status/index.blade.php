@extends('adminlte::page')
@section('title', 'All Personnel Status')

@section('content')
    <div class="box">
        <div class="box-header">
            <div class="col-md-6">
                <h3 class="box-title">All Personnel Status</h3>
            </div>
            <div class="col-md-6 text-right">
                <a href="{{ route('inactive-reasons.create') }}" class="btn btn-primary btnTheme">Create Personnel Status</a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="client" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Sr#</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @if(!empty($personal_statuses))
                    @foreach($personal_statuses as $personal_status)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $personal_status->name ?? '--' }}</td>
                            <td>
                                <a href="{{ route('personal-status.edit', $personal_status->id) }}"><button class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-pencil"></i></button></a>
                                {{--<a href="#"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>--}}
                                <form action="{{ route('personal-status.destroy', $personal_status->id) }}" method="POST" class="delete_item">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-xs btn-danger" data-toggle="tooltip" title="Delete"><i class="glyphicon glyphicon-remove"></i></button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

@endsection

@push('js')
    <script type="text/javascript">
        $(function () {
            $('#client').DataTable({
                'lengthChange': false,
            })

        });
    </script>
@endpush
