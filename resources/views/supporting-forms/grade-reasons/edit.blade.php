@extends('adminlte::page')
@section('title', 'Grade Reasons')

@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Edit Reasons Grade</h3>
        </div>
        <form action="{{ route('grade-reasons.update', $grade_reason->id) }}" method="POST">
            @csrf
            {{ method_field('PUT') }}
            <div class="box-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="{{ $grade_reason->name }}" required>
                            @if($errors->has('name'))
                                <span class="help-block text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary btnTheme">Submit</button>
            </div>
        </form>
    </div>
@endsection
